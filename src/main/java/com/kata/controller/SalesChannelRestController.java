package com.kata.controller;

import com.kata.dto.SalesChannelDTO;
import com.kata.service.SalesChannelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/catalog/saleschannel")
@Api(value = "/saleschannel", tags = "Канал продаж")
public class SalesChannelRestController {
    private final SalesChannelService service;

    @ApiOperation(value = "Получение всех Каналов продаж",
            notes = "Возвращает объекты всех Каналов продаж")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Все Каналы продаж получены")
    })
    @GetMapping
    public ResponseEntity<List<SalesChannelDTO>> getAllSalesChannels() {

        return new ResponseEntity<>(service.findAll(), HttpStatus.OK);
    }

    @ApiOperation(value = "Получение Канала продаж",
            notes = "Принимает id канала из URL и возвращает объект Канала продаж по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Канал продаж получен"),
            @ApiResponse(code = 404, message = "Канал продаж с таким id не найден")
    })
    @GetMapping("/{id}")
    public ResponseEntity<SalesChannelDTO> getSalesChannelById(@PathVariable("id") @ApiParam(name = "id"
            , value = "Id Канала продаж", example = "1") Long id) {

        return new ResponseEntity<>(service.findById(id), HttpStatus.OK);
    }

    @ApiOperation(value = "Создание нового Канала продаж",
            notes = "Принимает объект Канала продаж.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Создан новый Канал продаж"),
            @ApiResponse(code = 406, message = "Запись с таким Каналом продаж уже существует")
    })
    @PostMapping
    public ResponseEntity<HttpStatus> createSalesChannel(@RequestBody @ApiParam(name = "Объект"
            , value = "Объект Канала продаж для создания") SalesChannelDTO salesChannelDTO) {

        service.save(salesChannelDTO);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @ApiOperation(value = "Редактирование Канала продаж",
            notes = "Принимает id из URL и объект Канала продаж.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Канал продаж изменен"),
            @ApiResponse(code = 406, message = "Запись с таким Каналом продаж уже существует")
    })
    @PatchMapping("/{id}")
    ResponseEntity<HttpStatus> updateSalesChannel(@PathVariable("id") @ApiParam(name = "id"
            , value = "Id Канала продаж") Long id, @RequestBody @ApiParam(name = "Канал продаж"
            , value = "Объект Канала продаж для редактирования") SalesChannelDTO salesChannelDTO) {

        service.update(id, salesChannelDTO);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @ApiOperation(value = "Удаление Канала продаж",
            notes = "Принимает id Канала продаж из URL и удаляет Канал продаж по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Канал продаж удалён"),
            @ApiResponse(code = 404, message = "Канал продаж с таким id не найден")
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteSalesChannelById(@PathVariable("id") @ApiParam(name = "id"
            , value = "Id Канала продаж для удаления", example = "1") Long id) {

        service.delete(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }
}
