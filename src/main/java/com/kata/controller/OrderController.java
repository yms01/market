package com.kata.controller;

import com.kata.dto.document.OrderRequestDto;
import com.kata.dto.document.OrderResponseDto;
import com.kata.service.document.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
@Api(value = "/orders", tags = "Заказы")
@RequestMapping("/api/catalog/orders")
public class OrderController {

    private final OrderService service;

    @GetMapping()
    @ApiOperation(value = "Получение всех заказов")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Все заказы получены")
    })
    public ResponseEntity<List<OrderResponseDto>> allOrders() {
        return new ResponseEntity<>(service.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Получение заказа по ID из URL")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Заказ получен"),
            @ApiResponse(code = 404, message = "Заказ с таким id не найден")})
    public ResponseEntity<OrderResponseDto> getOrder(
            @ApiParam(name = "id", value = "ID заказа", example = "1")
            @PathVariable(value = "id") Long id) {
        OrderResponseDto dto = service.findById(id);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PostMapping()
    @ApiOperation(value = "Создание заказа")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Заказ создан"),
    })
    public ResponseEntity<HttpStatus> createOrder(
            @ApiParam(name = "Объект DTO",
                    value = "OrderRequestDto. Плановую дату приемки задавать в формате yyyy-MM-dd HH:mm." +
                            "Значение флага buying указывает является-ли заказ закупкой или продажей." +
                            "Номер заказа (поле number) может задаваться вручную или генерироваться автоматически")
            @RequestBody OrderRequestDto request) {
        service.save(request);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @PatchMapping("/{id}")
    @ApiOperation(value = "Изменение заказа")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Заказ изменен")
    })
    public ResponseEntity<HttpStatus> editOrder(
            @ApiParam(name = "id", value = "ID заказа из параметра запроса", example = "1")
            @PathVariable(value = "id") Long id,
            @ApiParam(name = "Объект DTO",
                    value = "OrderRequestDto. Плановую дату приемки задавать в формате yyyy-MM-dd HH:mm." +
                            "Значение флага buying указывает является-ли заказ закупкой или продажей." +
                            "Номер заказа (поле number) может задаваться вручную или генерироваться автоматически")
            @RequestBody OrderRequestDto request) {
        service.update(id, request);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Удаление заказа")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Заказ удален"),
            @ApiResponse(code = 404, message = "Заказ с таким id не найден")})
    public ResponseEntity<HttpStatus> deleteOrder(
            @ApiParam(name = "id",value = "ID заказа из параметра запроса", example = "1")
            @PathVariable(value = "id") Long id) {
        service.deleteById(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }
}

