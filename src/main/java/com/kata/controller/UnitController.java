package com.kata.controller;

import com.kata.dto.UnitRequestDto;
import com.kata.dto.UnitResponseDto;
import com.kata.service.UnitService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(value = "/units", tags = "Единицы измерения")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/catalog/units")
public class UnitController {

    private final UnitService service;

    @ApiOperation(value = "Получение всех единиц измерения")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Все единицы измерения получены")
    })
    @GetMapping()
    public ResponseEntity<List<UnitResponseDto>> allUnits() {
        return new ResponseEntity<>(service.getAllUnits(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Получение единицы измерения по ID из URL")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Единица измерения получена"),
            @ApiResponse(code = 404, message = "Единица измерения с таким id не найдена")
    })
    public ResponseEntity<UnitResponseDto> getUnit(
            @ApiParam(name = "id", value = "ID единицы измерения", example = "1")
            @PathVariable(value = "id") Long id) {
        UnitResponseDto unitDto = service.findById(id);
        return new ResponseEntity<>(unitDto, HttpStatus.OK);
    }

    @PostMapping()
    @ApiOperation(value = "Создание единицы измерения")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Пользовательская единица измерения создана"),
    })
    public ResponseEntity<HttpStatus> createUnit(
            @ApiParam(name = "Объект DTO",
                    value = "DTO с кратким, полным именем и цифровым кодом")
            @RequestBody UnitRequestDto request) {
        service.create(request);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @PatchMapping("/{id}")
    @ApiOperation(value = "Изменение цифрового кода, полного и краткого имени единицы измерения")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Единица измерения изменена")
    })
    public ResponseEntity<HttpStatus> editUnit(
            @ApiParam(name = "id", value = "ID единицы измерения из параметра запроса", example = "1")
            @PathVariable(value = "id") Long id,
            @ApiParam(name = "Объект DTO",
                    value = "DTO с кратким, полным именем и цифровым кодом")
            @RequestBody UnitRequestDto request) {
        service.edit(id, request);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Удаление единицы измерения")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Единица измерения удалена"),
            @ApiResponse(code = 404, message = "Единица измерения с таким id не найдена")})
    public ResponseEntity<HttpStatus> deleteUnit(
            @ApiParam(name = "id",value = "ID единицы измерения из параметра запроса", example = "1")
            @PathVariable(value = "id") Long id) {
        service.deleteById(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }

}
