package com.kata.controller;

import com.kata.dto.TaxationSystemDTO;
import com.kata.service.TaxationSystemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(value = "/taxation_system", tags = "Система налогообложения")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/catalog/taxation_system")
public class TaxationSystemRestController {

    private final TaxationSystemService taxationSystemService;

    @ApiOperation(value = "Получение всех Систем налогообложения",
            notes = "Возвращает объекты всех Систем налогообложения")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Все Системы налогообложения получены")
    })
    @GetMapping
    public ResponseEntity<List<TaxationSystemDTO>> getAllTaxationSystem() {

        return new ResponseEntity<>(taxationSystemService.findAll(), HttpStatus.OK);
    }

    @ApiOperation(value = "Получение Системы налогообложения",
            notes = "Принимает id Системы налогообложения из URL и возвращает объект Системы налогообложения по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Система налогообложения получена"),
            @ApiResponse(code = 404, message = "Система налогообложения с таким id не найдена")
    })
    @GetMapping("/{id}")
    public ResponseEntity<TaxationSystemDTO> getTaxationSystemById(@PathVariable("id") @ApiParam(name = "id"
            , value = "Id Системы налогообложения", example = "1") Long id) {

        return new ResponseEntity<>(taxationSystemService.findById(id), HttpStatus.OK);
    }

    @ApiOperation(value = "Создание Системы налогообложения",
            notes = "Принимает объект Системы налогообложения. Поле id должно быть null")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Система налогообложения создана"),
            @ApiResponse(code = 406, message = "Такая Система налогообложения уже существует")
    })
    @PostMapping
    public ResponseEntity<HttpStatus> createTaxationSystem(@RequestBody @ApiParam(name = "Объект"
            , value = "Объект Системы налогообложения для создания") TaxationSystemDTO taxationSystemDTO) {

        taxationSystemService.save(taxationSystemDTO);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @ApiOperation(value = "Редактирование Системы налогообложения",
            notes = "Принимает id из URL и объект Системы налогообложения. Поле id должно быть null")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Система налогообложения изменена"),
            @ApiResponse(code = 406, message = "Такая Система налогообложения уже существует")
    })
    @PatchMapping("/{id}")
    ResponseEntity<HttpStatus> updateTaxationSystem(@PathVariable("id") @ApiParam(name = "id"
            , value = "Id Системы налогообложения") Long id, @RequestBody @ApiParam(name = "Система налогообложения"
            , value = "Объект Системы налогообложения для редактирования") TaxationSystemDTO taxationSystemDTO) {

        taxationSystemService.update(id, taxationSystemDTO);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @ApiOperation(value = "Удаление Системы налогообложения",
            notes = "Принимает id Системы налогообложения из URL и удаляет Систему налогообложения по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Система налогообложения удалена"),
            @ApiResponse(code = 404, message = "Система налогообложения с таким id не найдена")
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteTaxationSystemById(@PathVariable("id") @ApiParam(name = "id"
            , value = "Id Системы налогообложения для удаления", example = "1") Long id) {

        taxationSystemService.deleteById(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }
}
