package com.kata.controller;

import com.kata.dto.TypeOfAccountingDto;
import com.kata.service.TypeOfAccountingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/catalog/typeOfAccounting")
@Api(value = "/typeOfAccounting", tags = "Тип учета")
public class TypeOfAccountingController {

    private final TypeOfAccountingService typeOfAccountingService;

    @ApiOperation(value = "Получение всех типов учета",
            notes = "Возвращает объекты всех типов учета")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Все тыпы учета получены")
    })
    @GetMapping
    public ResponseEntity<List<TypeOfAccountingDto>> getAllTypeOfAccounting() {
        return new ResponseEntity<>(typeOfAccountingService.findAll(), HttpStatus.OK);
    }

    @ApiOperation(value = "Получение всех типов учета",
            notes = "Принимает id ставки из URL и возвращает объект типа учета по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Типы учета получены"),
            @ApiResponse(code = 404, message = "Тип учета с таким id не найден")
    })
    @GetMapping("/{id}")
    public ResponseEntity<TypeOfAccountingDto> getTypeOfAccountingById(@PathVariable("id") @ApiParam(name = "id", value = "Id типа учета") Long id) {
        return new ResponseEntity<>(typeOfAccountingService.findById(id), HttpStatus.OK);
    }

    @ApiOperation(value = "Создание типа учета",
            notes = "Принимает объект типа учета. Поле id должн быть null")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Тип учета создан"),
            @ApiResponse(code = 406, message = "Запись с таким типом учета уже существует")
    })
    @PostMapping
    public ResponseEntity<HttpStatus> createTypeOfAccounting(@RequestBody @ApiParam(name = "Объект", value = "Объект типа учета для создания") TypeOfAccountingDto typeOfAccountingDto) {
        typeOfAccountingService.create(typeOfAccountingDto);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @ApiOperation(value = "Редактирование типа учета",
            notes = "Принимает id из URL и объект типа учета. Поле id должн быть null")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Тип учета изменен"),
            @ApiResponse(code = 406, message = "Запись с таким значением типа учета уже существует")
    })
    @PatchMapping("/{id}")
    ResponseEntity<HttpStatus> updateTypeOfAccounting(@PathVariable("id") @ApiParam(name = "id", value = "Id типа учета") Long id,
                                         @RequestBody @ApiParam(name = "Тип учета", value = "Объект типа учета для редактирования") TypeOfAccountingDto typeOfAccountingDto) {
        typeOfAccountingService.update(id, typeOfAccountingDto);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @ApiOperation(value = "Удаление типа учета",
            notes = "Принимает id ставки из URL и удаляет тип учета по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Тип учета удален"),
            @ApiResponse(code = 404, message = "Тип учета с таким id не найден")
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteTypeOfAccountingById(@PathVariable("id") @ApiParam(name = "id", value = "Id типа учета для удаления") Long id) {
        typeOfAccountingService.deleteById(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }
}
