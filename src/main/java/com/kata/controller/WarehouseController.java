package com.kata.controller;

import com.kata.dto.warehouse.WarehouseRequestDto;
import com.kata.dto.warehouse.WarehouseResponseDto;
import com.kata.service.warehouse.WarehouseService;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(value = "/warehouses", tags = "Склады")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/catalog/warehouses")
public class WarehouseController {

    private final WarehouseService service;

    @ApiOperation(value = "Получение всех складов")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Все склады получены")
    })
    @GetMapping()
    public ResponseEntity<List<WarehouseResponseDto>> allWarehouses() {
        return new ResponseEntity<>(service.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Получение склада по ID из URL")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Склад получен"),
            @ApiResponse(code = 404, message = "Склад с таким id не найдена")
    })
    public ResponseEntity<WarehouseResponseDto> getWarehouseById(
            @ApiParam(name = "id", value = "ID Склада", example = "1")
            @PathVariable(value = "id") Long id) {
        return new ResponseEntity<>(service.findById(id), HttpStatus.OK);
    }

    @PostMapping()
    @ApiOperation(value = "Создание Склада")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Пользовательский Склад создан"),
    })
    public ResponseEntity<HttpStatus> createWarehouse(
            @ApiParam(name = "Объект DTO", value = "DTO склада для запроса. " +
                    "parentId указывает на id другого существующего склада." +
                    "zoneId в сете ячеек должно соответствовать id существующих в данном складе зон.")
            @RequestBody WarehouseRequestDto request) {
        service.save(request);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @PatchMapping("/{id}")
    @ApiOperation(value = "Изменение параметров склада")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Склад изменен")
    })
    public ResponseEntity<HttpStatus> editWarehouse(
            @ApiParam(name = "id", value = "ID склада из параметра запроса", example = "1")
            @PathVariable(value = "id") Long id,
            @ApiParam(name = "Объект DTO",value = "DTO склада для запроса. " +
                    "parentId указывает на id другого существующего склада." +
                    "zoneId в сете ячеек должно соответствовать id существующих в данном складе зон.")
            @RequestBody WarehouseRequestDto request) {
        service.update(id, request);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Удаление склада")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Склад удален"),
            @ApiResponse(code = 404, message = "Склад с таким id не найден")})
    public ResponseEntity<HttpStatus> deleteWarehouse(
            @ApiParam(name = "id",value = "ID Склада из параметра запроса", example = "1")
            @PathVariable(value = "id") Long id) {
        service.deleteById(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }
}
