package com.kata.controller;

import com.kata.dto.CountryDTO;
import com.kata.service.CountryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ In the name of Allah, most gracious and most merciful! 16.11.2022
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/catalog/countries")
@Api(value = "/countries", tags = "Страны")
public class CountryController {
    private final CountryService countryService;

    @ApiOperation(value = "Получение всех стран",
            notes = "Возвращает объекты всех стран")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Все страны получены!")
    })
    @GetMapping
    public ResponseEntity<List<CountryDTO>> getCountries() {
        return new ResponseEntity<>(countryService.getAllCountries(), HttpStatus.OK);

    }

    @ApiOperation(value = "Получение страны",
            notes = "Принимает id страны из URL и возвращает объект страны по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Страна получена!"),
            @ApiResponse(code = 404, message = "Страна с таким id не найдена!")
    })
    @GetMapping(path = "{id}")
    public ResponseEntity<CountryDTO> getCountry(@PathVariable("id")
                                                 @ApiParam(name = "id", value = "Id страны", example = "1")
                                                 Long countryId) {
        return new ResponseEntity<>(countryService.getCountry(countryId), HttpStatus.OK);
    }

    @ApiOperation(value = "Создание страны",
            notes = "Принимает объект страна. Поле id должно быть null")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Страна создана!"),
            @ApiResponse(code = 406, message = "Такая страна уже существует!")
    })
    @PostMapping
    public ResponseEntity<HttpStatus> addNewCountry(@RequestBody
                                                    @ApiParam(name = "Объект", value = "Объект страна для создания")
                                                    CountryDTO countryDTO) {
        countryService.addCountry(countryDTO);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @ApiOperation(value = "Редактирование страны",
            notes = "Принимает из URL id и страну. Поле id должно быть null")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Страна изменена!"),
            @ApiResponse(code = 406, message = "Такая страна уже существует!")
    })
    @PutMapping("/{id}")
    ResponseEntity<HttpStatus> updateCountry(@PathVariable("id")
                                             @ApiParam(name = "id", value = "страна") Long id,
                                             @RequestBody @ApiParam(name = "Страна",
                                                     value = "Страна для редактирования") CountryDTO countryDTO) {
        countryService.update(id, countryDTO);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @ApiOperation(value = "Удаление страны",
            notes = "Принимает из URL id страны и удаляет страну по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Страна удалена!"),
            @ApiResponse(code = 404, message = "Страна с таким id не найдена!")
    })
    @DeleteMapping(path = "{id}")
    public ResponseEntity<HttpStatus> deleteCountry(@PathVariable("id")
                                                    @ApiParam(name = "id", value = "Id страны для удаления", example = "1")
                                                    Long countryId) {
        countryService.deleteCountry(countryId);
        return ResponseEntity.ok(HttpStatus.OK);
    }
}
