package com.kata.controller;

import com.kata.dto.ProductTypeRequestDTO;
import com.kata.dto.ProductTypeResponseDTO;
import com.kata.service.ProductTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ In the name of Allah, most gracious and most merciful! 03.12.2022
 */
@RestController
@RequiredArgsConstructor
@Api(value = "/product_types", tags = "Тип продукции")
@RequestMapping("/api/catalog/product_types")
public class ProductTypeRestController {
    private final ProductTypeService productTypeService;

    @ApiOperation(value = "Получение всех типов продукции",
            notes = "Возвращает объекты всех типов продукции")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Все типы продукции получены!")
    })
    @GetMapping
    public ResponseEntity<List<ProductTypeResponseDTO>> getProductTypes() {
        return new ResponseEntity<>(productTypeService.getAllProductTypes(), HttpStatus.OK);
    }

    @ApiOperation(value = "Получение типа продукции",
            notes = "Принимает id типа продукции из URL и возвращает объект типа продукции по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Тип продукции получена!"),
            @ApiResponse(code = 404, message = "Тип продукции с таким id не найден!")
    })
    @GetMapping(path = "{id}")
    public ResponseEntity<ProductTypeResponseDTO> getProductType(@PathVariable("id")
                                                                 @ApiParam(
                                                                         name = "id",
                                                                         value = "Id типа продукции",
                                                                         example = "1"
                                                                 )
                                                                 Long id) {
        return new ResponseEntity<>(productTypeService.getProductType(id), HttpStatus.OK);
    }

    @ApiOperation(value = "Создание типа продукции!",
            notes = "Принимает объект тип продукции. Поле id должно быть null")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Тип продукции создана!"),
            @ApiResponse(code = 406, message = "Такой тип продукции уже существует!")
    })
    @PostMapping
    public ResponseEntity<HttpStatus> addNewProductType(@RequestBody
                                                        @ApiParam(
                                                                name = "Объект",
                                                                value = "Объект тип продукции для создания"
                                                        )
                                                        ProductTypeRequestDTO productTypeRequestDTO) {
        productTypeService.addProductType(productTypeRequestDTO);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @ApiOperation(value = "Редактирование типа продукции",
            notes = "Принимает из URL id и тип продукции. Поле id должно быть null")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Тип продукции изменен!"),
            @ApiResponse(code = 406, message = "Такой тип продукции уже существует!")
    })
    @PutMapping("/{id}")
    ResponseEntity<HttpStatus> updateCountry(@PathVariable("id")
                                             @ApiParam(name = "id", value = "тип продукции") Long id,
                                             @RequestBody @ApiParam(
                                                     name = "Тип продукции",
                                                     value = "Объект типа продукции для редактирования"
                                             )
                                             ProductTypeRequestDTO productTypeRequestDTO) {
        productTypeService.updateProductType(id, productTypeRequestDTO);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @ApiOperation(value = "Удаление типа продукции",
            notes = "Принимает из URL id типа продукции и удаляет тип продукции по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Тип продукции удален!"),
            @ApiResponse(code = 404, message = "Тип продукции с таким id не найден!")
    })
    @DeleteMapping(path = "{id}")
    public ResponseEntity<HttpStatus> deleteCountry(@PathVariable("id")
                                                    @ApiParam(
                                                            name = "id",
                                                            value = "Id типа продукции для удаления",
                                                            example = "1")
                                                    Long id) {
        productTypeService.deleteProductType(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }
}