package com.kata.controller;

import com.kata.dto.AddressRequestDto;
import com.kata.dto.AddressResponseDto;
import com.kata.service.AddressService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/catalog/addresses")
@Api(value = "/addresses", tags = "Почтовые адресы")
public class AddressRestController {

    private final AddressService addressService;

    @ApiOperation(value = "Получение всех почтовых адресов",
            notes = "Возвращает объекты всех почтовых адресов")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Все почтовые адресы получены")
    })
    @GetMapping
    public ResponseEntity<List<AddressResponseDto>> getAllAddresses() {
        return new ResponseEntity<>(addressService.findAll(), HttpStatus.OK);
    }

    @ApiOperation(value = "Получение почтового адреса",
            notes = "Принимает id адреса и врзвращает объект адреса по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Адрес получен"),
            @ApiResponse(code = 404, message = "Адрес с таким id не найден")
    })
    @GetMapping("/{id}")
    public ResponseEntity<AddressResponseDto> getAddressById(@PathVariable("id") @ApiParam(name = "id", value = "Id адреса") Long id) {
        return new ResponseEntity<>(addressService.findById(id), HttpStatus.OK);
    }


    @ApiOperation(value = "Создание почтового адреса",
            notes = "Принимает объект адреса")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Адрес сохранён"),
            @ApiResponse(code = 406, message = "Такой адрес уже существует")
    })
    @PostMapping
    public ResponseEntity<HttpStatus> createAddress(@RequestBody @ApiParam(name = "Адрес", value = "Объект адреса для сохранения") AddressRequestDto addressRequestDto) {
        addressService.save(addressRequestDto);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @ApiOperation(value = "Редактирование почтового адреса",
            notes = "Принимает id из URL и объект почтового адреса")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Изменения в почтовом адресе сохранены"),
            @ApiResponse(code = 406, message = "Такой адрес уже существует")
    })
    @PatchMapping("/{id}")
    ResponseEntity<HttpStatus> updateAddress(@PathVariable("id") @ApiParam(name = "id", value = "Id адреса") Long id,
                                             @RequestBody @ApiParam(name = "Адрес", value = "Объект адреса для редактирования") AddressRequestDto addressRequestDto) {
        addressService.update(id, addressRequestDto);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @ApiOperation(value = "Удаление почтового адреса",
            notes = "Принимает id из URL и удаляет почтовый адрес")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Адрес удалён"),
            @ApiResponse(code = 404, message = "Адрес с таким id не найден")
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteAddressById(@PathVariable("id") @ApiParam(name = "id", value = "Id адреса для удаления") Long id) {
        addressService.delete(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }

}
