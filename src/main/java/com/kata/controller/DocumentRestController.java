package com.kata.controller;

import com.kata.dto.document.DocumentDTO;
import com.kata.service.document.DocumentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/document")
@Api(value = "/document", tags = "Документ")
public class DocumentRestController {

    private final DocumentService documentService;

    @Autowired
    public DocumentRestController(DocumentService documentService) {
        this.documentService = documentService;
    }

    @ApiOperation(value = "Получить все документы", notes = "Возвращает объекты всех документов")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Все документы получены")
    })
    @GetMapping()
    public ResponseEntity<List<DocumentDTO>> getDocuments() {
        return new ResponseEntity<>(documentService.getAllDocuments(), HttpStatus.OK);
    }

    @ApiOperation(value = "Получить выбранный документ", notes = "принимает id документа и возвращает объект")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Документ получен"),
            @ApiResponse(code = 404, message = "Документ с таким id не найден")
    })
    @GetMapping("/{id}")
    public ResponseEntity<DocumentDTO> getDocumentById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(documentService.showDocument(id), HttpStatus.OK);
    }

    @ApiOperation(value = "Редактирование документа", notes = "Принимает id из URL и документ.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Документ изменен"),
            @ApiResponse(code = 406, message = "Такой документ уже существует")
    })
    @PatchMapping("/{id}")
    public ResponseEntity<HttpStatus> updateDocument(@PathVariable("id") @ApiParam(name = "id"
            , value = "Документ") Long id, @RequestBody @ApiParam(name = "Документ"
            , value = "Документ для редактирования") DocumentDTO documentDTO) {

        documentService.updateDocument(id, documentDTO);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @ApiOperation(value = "Создание документа", notes = "Принимает объект документ. Поле id генерируется автоматически")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Документ создан"),
            @ApiResponse(code = 406, message = "Такой документ уже существует")
    })
    @PostMapping()
    public ResponseEntity<HttpStatus> createDocument(@RequestBody @ApiParam(name = "Объект"
            , value = "Объект документ для создания") DocumentDTO documentDTO) {
        documentService.saveDocument(documentDTO);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @ApiOperation(value = "Удаление документа", notes = "Принимает id документа из URL и удаляет документ по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Документ удален"),
            @ApiResponse(code = 404, message = "Документ с таким id не найден")
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteDocument(@PathVariable("id") Long id) {
        documentService.deleteDocument(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }
}
