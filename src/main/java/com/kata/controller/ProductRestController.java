package com.kata.controller;

import com.kata.dto.AddressRequestDto;
import com.kata.dto.AddressResponseDto;
import com.kata.dto.product.ProductRequestDto;
import com.kata.dto.product.ProductResponseDto;
import com.kata.service.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/all-products")
@Api(value = "/all-products", tags = "Товары")
public class ProductRestController {

    private final ProductService productService;

    @ApiOperation(value = "Получение товаров всех видов",
            notes = "Возвращает объекты всех товаров, товаров-услуг, товаров-комплектов")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Все товары получены")
    })
    @GetMapping
    public ResponseEntity<List<ProductResponseDto>> getAll() {
        return new ResponseEntity<>(productService.findAll(), HttpStatus.OK);
    }

    @ApiOperation(value = "Получение всех товаров, не являющихся услугами или комплектами",
            notes = "Возвращает объекты всех товаров")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Все товары получены")
    })
    @GetMapping("/products")
    public ResponseEntity<List<ProductResponseDto>> getAllProducts() {
        return new ResponseEntity<>(productService.findAllProducts(), HttpStatus.OK);
    }

    @ApiOperation(value = "Получение всех товаров, являющихся услугами",
            notes = "Возвращает объекты всех товаров-услуг")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Все товары-услуги получены")
    })
    @GetMapping("/services")
    public ResponseEntity<List<ProductResponseDto>> getAllServices() {
        return new ResponseEntity<>(productService.findAllServices(), HttpStatus.OK);
    }

    @ApiOperation(value = "Получение всех товаров, являющихся комплектами",
            notes = "Возвращает объекты всех товаров-комплектов")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Все товары-комплекты получены")
    })
    @GetMapping("/sets")
    public ResponseEntity<List<ProductResponseDto>> getAllSets() {
        return new ResponseEntity<>(productService.findAllSets(), HttpStatus.OK);
    }

    @ApiOperation(value = "Получение товара, товара-услуги или товара-комплекта",
            notes = "Принимает id и возвращает объект товара")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Товар получен"),
            @ApiResponse(code = 404, message = "Товар с таким id не найден")
    })
    @GetMapping("/{id}")
    public ResponseEntity<ProductResponseDto> getById(@PathVariable("id") @ApiParam(name = "id", value = "Id товара") Long id) {
        return new ResponseEntity<>(productService.findById(id), HttpStatus.OK);
    }

    @ApiOperation(value = "Создание товара, товара-услуги или товара-комплекта",
            notes = "Принимает объект товара")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Товар сохранён")
    })
    @PostMapping
    public ResponseEntity<HttpStatus> createAddress(@RequestBody @ApiParam(name = "Товар", value = "Объект товара для сохранения") ProductRequestDto productRequestDto) {
        productService.save(productRequestDto);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @ApiOperation(value = "Редактирование товара, товара-услуги или товара-комплекта",
            notes = "Принимает id из URL и объект товара")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Изменения в товаре сохранены"),
            @ApiResponse(code = 406, message = "Такой товар уже существует")
    })
    @PatchMapping("/{id}")
    ResponseEntity<HttpStatus> updateById(@PathVariable("id") @ApiParam(name = "id", value = "Id товара") Long id,
                                             @RequestBody @ApiParam(name = "Товар", value = "Объект товара для редактирования") ProductRequestDto productRequestDto) {
        productService.update(id, productRequestDto);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @ApiOperation(value = "Удаление товара, товара-услуги или товара-комплекта",
            notes = "Принимает id из URL и удаляет товар")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Товар удалён"),
            @ApiResponse(code = 404, message = "Товар с таким id не найден")
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteById(@PathVariable("id") @ApiParam(name = "id", value = "Id товара для удаления") Long id) {
        productService.delete(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }

}
