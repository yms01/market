package com.kata.controller;

import com.kata.dto.CounterpartyDto;
import com.kata.service.CounterpartyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/catalog/counterparty")
@Api(value = "/counterparty", tags = "Counterparty controller")
public class СounterpartyController {

    private final CounterpartyService counterpartyService;

    @GetMapping
    @ApiOperation(value = "Получение всех контрагентов",
            notes = "Принимает всех контрагентов")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Все контрагенты получены"),
            @ApiResponse(code = 404, message = "Все id контрагентов не найдены")
    })
    public ResponseEntity<List<CounterpartyDto>> getAllCounterparty() {
        return new ResponseEntity<>(counterpartyService.findAll(), HttpStatus.OK);
    }

    @ApiOperation(value = "Получение контрагентов",
            notes = "Принимает id контрагентов из URL и врзвращает объект контрагент по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Контрагент получен"),
            @ApiResponse(code = 404, message = "Контрагент с таким id не найден")
    })
    @GetMapping("/{id}")
    public ResponseEntity<CounterpartyDto> getById(@PathVariable("id") @ApiParam(name = "id", value = "Id контрагента") Long id) {
        return new ResponseEntity<>(counterpartyService.findById(id), HttpStatus.OK);
    }

    @ApiOperation(value = "Создание пользователя контрагента",
            notes = "Помечает на создание контрагента")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Контрагент создан"),
            @ApiResponse(code = 406, message = "Запись с таким значением контрагента уже существует")
    })
    @PostMapping
    public ResponseEntity<HttpStatus> createCounterparty(@RequestBody @ApiParam(name = "Объект", value = "Объект контрагент для создания") CounterpartyDto counterpartyDto) {
        counterpartyService.create(counterpartyDto);
        return ResponseEntity.ok( HttpStatus.CREATED);
    }

    @ApiOperation(value = "Редактирование контрагента",
            notes = "Принимает id из URL и объект контрагент")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Контрагентн изменен"),
            @ApiResponse(code = 406, message = "Запись с таким контрагентом уже существует")
    })
    @PatchMapping("/{id}")
    ResponseEntity<HttpStatus> updateCounterparty(@PathVariable("id") @ApiParam(name = "id", value = "Id контрагента") Long id,
                                                  @RequestBody @ApiParam(name = "Контрагент", value = "Объект контрагент для редактирования") CounterpartyDto counterpartyDto) {
        counterpartyService.update(id, counterpartyDto);
        return  ResponseEntity.ok(HttpStatus.OK);
    }

    @ApiOperation(value = "Удаление контрагента",
            notes = "Принимает id контрагента из URL и удаляет контрагента по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Контрагент удален"),
            @ApiResponse(code = 404, message = "Контрагент с таким id не найден")
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteCounterpartyById(@PathVariable("id") @ApiParam(name = "id", value = "Id контрагента для удаления") Long id) {
        counterpartyService.deleteById(id);
        return ResponseEntity.ok(HttpStatus.OK);
    }
}