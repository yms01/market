package com.kata.mapper;

import com.kata.dto.TypeOfAccountingDto;
import com.kata.model.TypeOfAccounting;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface TypeOfAccountingMapper {

    TypeOfAccountingMapper INSTANCE = Mappers.getMapper(TypeOfAccountingMapper.class);

    List<TypeOfAccountingDto> toDtoList(List<TypeOfAccounting> typeOfAccountings);

    TypeOfAccountingDto toDto(TypeOfAccounting typeOfAccounting);

    TypeOfAccounting toEntity(TypeOfAccountingDto typeOfAccountingDto);
}
