package com.kata.mapper;

import com.kata.dto.CounterpartyDto;
import com.kata.model.Counterparty;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface CounterpartyMapper {

    CounterpartyMapper INSTANCE = Mappers.getMapper(CounterpartyMapper.class);

    List<CounterpartyDto> toDtoList(List<Counterparty> counterparties);

    CounterpartyDto toDto(Counterparty counterparty);

    Counterparty toEntity(CounterpartyDto counterpartyDto);

}
