package com.kata.mapper.warehouse;

import com.kata.dto.warehouse.ZoneRequestDto;
import com.kata.model.warehouse.Zone;
import org.mapstruct.Mapper;

import java.util.Set;

@Mapper(componentModel = "spring")
public interface ZoneMapper {
    Set<Zone> toZoneSet(Set<ZoneRequestDto> zoneDtoSet);

}
