package com.kata.mapper.document;

import com.kata.dto.document.InvoiceDTO;
import com.kata.model.document.Invoice;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface InvoiceMapper {

    InvoiceMapper INSTANCE = Mappers.getMapper(InvoiceMapper.class);

    List<InvoiceDTO> toDtoList(List<Invoice> invoices);

    InvoiceDTO toDto(Invoice invoice);

    Invoice toEntity(InvoiceDTO invoiceFromSupplierDTO);
}
