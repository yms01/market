package com.kata.mapper.document;

import com.kata.dto.document.OrderRequestDto;
import com.kata.dto.document.OrderResponseDto;
import com.kata.model.document.Order;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface OrderMapper {

    @Mapping(target = "isRemoved", constant = "false")
    Order toEntity(OrderRequestDto dto);

    OrderResponseDto toDto(Order order);

    List<OrderResponseDto> toDtoList(List<Order> orders);

}
