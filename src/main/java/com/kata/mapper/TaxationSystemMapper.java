package com.kata.mapper;

import com.kata.dto.TaxationSystemDTO;
import com.kata.model.TaxationSystem;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TaxationSystemMapper {
    TaxationSystemMapper INSTANCE = Mappers.getMapper(TaxationSystemMapper.class);

    TaxationSystem toEntity(TaxationSystemDTO taxationSystemDTO);

    TaxationSystemDTO toDto(TaxationSystem taxationSystem);

    List<TaxationSystemDTO> toDtoList(List<TaxationSystem> taxationSystems);
}
