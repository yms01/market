package com.kata.mapper;

import com.kata.dto.AddressRequestDto;
import com.kata.dto.AddressResponseDto;
import com.kata.model.Address;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface AddressMapper {

    AddressMapper INSTANCE = Mappers.getMapper(AddressMapper.class);

    List<AddressResponseDto> toDtoList(List<Address> addresses);

    AddressResponseDto toDto(Address address);

    Address toEntity(AddressRequestDto addressRequestDto);
}
