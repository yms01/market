package com.kata.mapper;



import com.kata.dto.YurlizoDto;

import com.kata.model.Yurlizo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface YurlizoMapper {

    YurlizoMapper INSTANCE = Mappers.getMapper(YurlizoMapper.class);
    YurlizoDto toDto(Yurlizo yurlizo);
    Yurlizo toEntity(YurlizoDto yurlizoDto);

    List<Yurlizo> toDtoList(List<Yurlizo> yurliza);
}
