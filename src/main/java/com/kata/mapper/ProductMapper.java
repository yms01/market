package com.kata.mapper;

import com.kata.dto.product.ProductRequestDto;
import com.kata.dto.product.ProductResponseDto;
import com.kata.model.Product;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface ProductMapper {

    ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class);

    List<ProductResponseDto> toDtoList(List<Product> products);

    ProductResponseDto toDto(Product product);

    Product toEntity(ProductRequestDto productRequestDto);
}
