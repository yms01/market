package com.kata.mapper;

import com.kata.dto.VatRequestDto;
import com.kata.dto.VatResponseDto;
import com.kata.model.Vat;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface VatMapper {

    VatMapper INSTANCE = Mappers.getMapper(VatMapper.class);

    List<VatResponseDto> toDtoList(List<Vat> vats);

    VatResponseDto toDto(Vat vat);

    Vat toEntity(VatRequestDto vatResponseDto);
}
