package com.kata.mapper;


import com.kata.dto.SignOfTheSubjectOfCalculationDto;
import com.kata.model.SignOfTheSubjectOfCalculation;
import org.mapstruct.factory.Mappers;

import java.util.List;

public interface SignOfTheSubjectOfCalculationMapper {


    SignOfTheSubjectOfCalculationMapper INSTANCE = Mappers.getMapper(
            SignOfTheSubjectOfCalculationMapper.class);
    SignOfTheSubjectOfCalculationDto toDto(
            SignOfTheSubjectOfCalculation signOfTheSubjectOfCalculation);
    SignOfTheSubjectOfCalculation toEntity(
            SignOfTheSubjectOfCalculationDto signOfTheSubjectOfCalculationDto);

    List<SignOfTheSubjectOfCalculation> toDtoList(List<SignOfTheSubjectOfCalculation> signOfTheSubjectOfCalculations);

}
