package com.kata.mapper;

import com.kata.dto.PackagingDTO;
import com.kata.model.Packaging;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import java.util.List;

@Mapper(componentModel = "spring")
public interface PackagingMapper {

    PackagingMapper INSTANCE = Mappers.getMapper(PackagingMapper.class);

    PackagingDTO modelToDto(Packaging packaging);

    List<PackagingDTO> modelsToDTOs(List<Packaging> packaging);

    Packaging dtoToModel(PackagingDTO packagingDTO);
}
