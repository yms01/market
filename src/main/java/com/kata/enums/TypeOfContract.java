package com.kata.enums;

public enum TypeOfContract{
    PURCHASE_AND_SALE, COMMISSION
}