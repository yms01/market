package com.kata.dto.warehouse;

import com.kata.model.warehouse.Cell;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class ZoneResponseDto {
    private Long id;

    private String zoneName;

    private Set<Cell> cells;

    private Integer totalCells;

    private Integer freeCells;

    private Integer occupiedCells;
}
