package com.kata.dto.warehouse;

import com.kata.dto.AddressResponseDto;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Set;

@Getter
@Setter
public class WarehouseResponseDto {
    private Long id;

    private String warehouseName;

    private AddressResponseDto address;

    private String comment;

    private Integer code;

    private Long parentId;

    private List<WarehouseResponseDto> children;

    private Integer externalCode;

    private Set<ZoneResponseDto> zones;

    private Set<CellResponseDto> cells;

}
