package com.kata.dto.warehouse;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CellRequestDto {

    private String cellName;

    private Long zoneId;
}
