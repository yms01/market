package com.kata.dto.warehouse;

import com.kata.model.warehouse.Zone;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CellResponseDto {

    private Long id;

    private String cellName;

    private Zone zone;

    private boolean isFree;
}
