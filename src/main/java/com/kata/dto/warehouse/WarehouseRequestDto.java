package com.kata.dto.warehouse;

import com.kata.dto.AddressRequestDto;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class WarehouseRequestDto {

    private String warehouseName;

    private AddressRequestDto address;

    private String comment;

    private Integer code;

    private Long parentId;

    private Integer externalCode;

    private Set<ZoneRequestDto> zones;

    private Set<CellRequestDto> cells;

}
