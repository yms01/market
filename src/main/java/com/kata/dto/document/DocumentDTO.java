package com.kata.dto.document;

import com.kata.model.Counterparty;
import com.kata.model.Currency;
import com.kata.model.Employee;
import com.kata.model.SalesChannel;
import com.kata.model.Status;
import com.kata.model.warehouse.Warehouse;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class DocumentDTO {

    private Long id;

    private boolean isRemoved;

    private LocalDateTime lastEdited;

    private Long number;

    private Currency currency;

    private Warehouse fromWarehouse;

    private Counterparty counterparty;

    private SalesChannel salesChannel;

    private boolean generalAccess;

    private Status status;

    private String comment;

    private LocalDateTime whenWasCreated;

    private Employee lastEditedBy;
}
