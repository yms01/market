package com.kata.dto.document;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.kata.dto.AddressResponseDto;
import com.kata.dto.YurlizoDto;
import com.kata.model.Product;
import com.kata.model.Project;
import com.kata.model.document.Contract;
import com.kata.model.document.File;
import com.kata.model.document.Task;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
public class OrderRequestDto extends DocumentDTO {

    private YurlizoDto organization;

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime dateOfAcceptance;

    private Contract contract;

    private Project project;

    private AddressResponseDto address;

    private boolean conducted;

    private boolean reserve;

    private boolean expectation;

    private Integer externalCode;

    private List<Product> products;

    private List<Task> tasks;

    private List<File> files;

    private boolean buying;

}
