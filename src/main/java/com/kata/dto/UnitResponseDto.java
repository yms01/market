package com.kata.dto;

import com.kata.model.Employee;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class UnitResponseDto {
    private Long id;

    private String shortName;

    private String fullName;

    private int digitalCode;

    private boolean isChangeable;

    private LocalDateTime date;

    private Employee editedBy;
}
