package com.kata.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TaxationSystemDTO {

    private Long id;
    private String name;
}
