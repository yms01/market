package com.kata.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VatRequestDto {

    private Byte rate;

    private String comment;

}
