package com.kata.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PackagingDTO {

    private Long id;

    private String packagingType;

    private Boolean isRemoved;
}
