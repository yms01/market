package com.kata.dto;

import com.kata.model.TypeOfBarcode;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class BarcodeDTO {

    private Long idBarcode;

    private TypeOfBarcode typeOfBarcode;

    private String nameBarcode;
}
