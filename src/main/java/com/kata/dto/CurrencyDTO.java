package com.kata.dto;

import lombok.Data;

@Data
public class CurrencyDTO {

    private int id;

    private boolean isRemoved;

    private boolean accountingCurrency;

    private String shortName;

    private String fullName;

    private int digitalCode;

    private String alphabeticCode;

    private double rate;

    private String TimeWhenWasChanged;
}
