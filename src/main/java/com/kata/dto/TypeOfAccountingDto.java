package com.kata.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TypeOfAccountingDto {

    private Long id;
    private String name;
    private Boolean isRemoved;
}
