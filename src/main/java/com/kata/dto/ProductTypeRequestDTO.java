package com.kata.dto;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Size;

/**
 * @ In the name of Allah, most gracious and most merciful! 02.12.2022
 */
@Getter
@Setter
public class ProductTypeRequestDTO {
    @NotNull
    @Size(max = 255)
    private String name;
}