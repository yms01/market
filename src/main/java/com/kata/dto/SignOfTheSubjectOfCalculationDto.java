package com.kata.dto;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SignOfTheSubjectOfCalculationDto {

    private Long id;

    private String name;

    private int artikul;

    private int code;

    private int vneshcode;

    private Boolean isRemoved;

}
