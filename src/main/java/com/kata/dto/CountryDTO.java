package com.kata.dto;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Size;

/**
 * @ In the name of Allah, most gracious and most merciful! 17.11.2022
 */
@Getter
@Setter
public class CountryDTO {
    private Long id;
    @NotNull
    private Boolean isSystem;
    @NotNull
    @Size(max = 255)
    private String shortName;
    @NotNull
    @Size(max = 255)
    private String fullName;
    @NotNull
    @Size(max = 3)
    private Integer numericCode;
    @NotNull
    @Size(max = 2)
    private String alpha2;
    @NotNull
    @Size(max = 3)
    private String alpha3;
    @NotNull
    private Boolean isRemoved;
}