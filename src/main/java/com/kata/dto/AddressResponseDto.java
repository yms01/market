package com.kata.dto;

import com.kata.model.Country;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddressResponseDto {

    private Long id;

    private String postalCode;

    private Country country;

    private String region;

    private String city;

    private String street;

    private String buildingNumber;

    private String apartmentNumber;

    private String other;

    private String addressComment;

    private Boolean isRemoved;
}
