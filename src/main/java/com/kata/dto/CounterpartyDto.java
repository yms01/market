package com.kata.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CounterpartyDto {

    private Long id;

    private String status;

    private String groups;

    private Long telephoneNumber;

    private Long fax;

    private String emailAddress;

    private String actualAddress;

    private String commentToTheAddress;

    private String comment;

    private Long code;

    private Long externalCode;

    private Boolean isRemoved;

}