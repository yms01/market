package com.kata.service;

import com.kata.dto.EmployeeDTO;

import java.util.List;

public interface EmployeeService {

    List<EmployeeDTO> findAll();

    EmployeeDTO findById(Long id);

    void save(EmployeeDTO employeeDTO);

    void update(Long id, EmployeeDTO employeeDTO);

    void deleteById(Long id);
}
