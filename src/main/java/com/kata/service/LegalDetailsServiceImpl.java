package com.kata.service;

import com.kata.dto.LegalDetailsDto;
import com.kata.mapper.LegalDetailsMapper;
import com.kata.model.LegalDetails;
import com.kata.repository.LegalDetailsRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@Slf4j

public class LegalDetailsServiceImpl implements LegalDetailsService {
    private final LegalDetailsRepository legalDetailsRepository;


    public LegalDetailsServiceImpl(LegalDetailsRepository legalDetailsRepository) {
        this.legalDetailsRepository = legalDetailsRepository;
    }

    @Override
    public List<LegalDetails> findAll() {
        log.info("All LEGAL DETAILS rates was found");
        return LegalDetailsMapper.INSTANCE.toDtoList(legalDetailsRepository.findAllNotRemoved());
    }

    @Override
    public LegalDetails findById(Long id) {
        log.info("LegalDetails  with id = {} was found", id);
        return legalDetailsRepository.findLegalDetailsByIdIfNotRemoved(id);
    }

    @Override
    public Object getById(Long id) {
        log.info("All LEGAL DETAILS  was found");
        return LegalDetailsMapper.INSTANCE.toDto(legalDetailsRepository.findLegalDetailsByIdIfNotRemoved(id));
    }

    @Override
    public void create(LegalDetailsDto legalDetailsDto) {
        LegalDetails legalDetails = LegalDetailsMapper.INSTANCE.toEntity(legalDetailsDto);
        legalDetails.setRemoved(false);
        legalDetailsRepository.save(legalDetails);
        log.info("All LEGAL DETAILS  was created");


    }

    @Override
    public void update(Long id, LegalDetailsDto legalDetailsDto) {
        LegalDetails fromDto = LegalDetailsMapper.INSTANCE.toEntity(legalDetailsDto);
        LegalDetails newLegalDetails = legalDetailsRepository.findLegalDetailsByIdIfNotRemoved(id);
        newLegalDetails.setRemoved(false);
        newLegalDetails.setTypeOfCounterparty(fromDto.getTypeOfCounterparty());
        newLegalDetails.setInn(fromDto.getInn());
        newLegalDetails.setFullName(fromDto.getFullName());
        newLegalDetails.setLegalAddress(fromDto.getLegalAddress());
        newLegalDetails.setKpp(fromDto.getKpp());
        newLegalDetails.setOgrn(fromDto.getOgrn());
        newLegalDetails.setOkpo(fromDto.getOkpo());
        legalDetailsRepository.save(newLegalDetails);
        log.info("All LEGAL DETAILS  was updated");



    }

    @Override
    public void delete(Long id) {
        legalDetailsRepository.markLegalDetailsAsRemoved(id);
        log.info("LEGAL DETAILS with id = {} was marked as removed", id);

    }
}
