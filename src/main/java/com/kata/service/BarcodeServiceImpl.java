package com.kata.service;


import com.kata.dto.BarcodeDTO;
import com.kata.mapper.BarcodeMapper;
import com.kata.model.Barcode;
import com.kata.repository.BarcodeRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class BarcodeServiceImpl implements BarcodeService {

    private final BarcodeRepository barcodeRepository;

    @Override
    public List<BarcodeDTO> findAll() {
        log.info("All barcodes was found");
        return BarcodeMapper.INSTANCE.toDtoList(barcodeRepository.findAllNotRemoved());
    }

    @Override
    public BarcodeDTO findById(Long idBarcode) {
        log.info("Barcode rate with id = {} was found", idBarcode);
        return BarcodeMapper.INSTANCE.toDto(barcodeRepository.findBarcodeByIdIfNotRemoved(idBarcode));
    }

    @Override
    @Transactional
    public void save(BarcodeDTO barcodeDTO) {
        Barcode barcode = BarcodeMapper.INSTANCE.toEntity(barcodeDTO);
        barcode.setRemoved(false);
        barcodeRepository.save(barcode);
        log.info("Barcode rate was saved: {}", barcode);

    }

    @Override
    @Transactional
    public void update(Long idBarcode, BarcodeDTO barcodeDTO) {
        Barcode fromDTO = BarcodeMapper.INSTANCE.toEntity(barcodeDTO);
        Barcode newBarcode = barcodeRepository.findBarcodeByIdIfNotRemoved(idBarcode);
        newBarcode.setRemoved(false);
        newBarcode.setNameBarcode(fromDTO.getNameBarcode());
        newBarcode.setTypeOfBarcode(fromDTO.getTypeOfBarcode());
        barcodeRepository.save(newBarcode);
        log.info("Barcode rate was updated: {}", newBarcode);
    }

    @Override
    @Transactional
    public void deleteById(Long idBarcode) {
        barcodeRepository.markBarcodeAsRemoved(idBarcode);
        log.info("Barcode rate with id = {} was marked as deleted", idBarcode);

    }
}
