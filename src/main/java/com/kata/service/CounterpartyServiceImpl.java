package com.kata.service;

import com.kata.dto.CounterpartyDto;
import com.kata.mapper.CounterpartyMapper;
import com.kata.model.Counterparty;
import com.kata.repository.CounterpartyRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class CounterpartyServiceImpl implements CounterpartyService {

    private final CounterpartyRepository counterpartyRepository;

    @Override
    public List<CounterpartyDto> findAll() {
        log.info("All COUNTERPARTY  was found");
        return CounterpartyMapper.INSTANCE.toDtoList(counterpartyRepository.findAllNotRemoved());
    }

    @Override
    public CounterpartyDto findById(Long id) {
        log.info("COUNTERPARTY with id = {} was found", id);
        return CounterpartyMapper.INSTANCE.toDto(counterpartyRepository.findCounterpartyByIdIfNotRemoved(id));
    }

    @Override
    @Transactional
    public void create(CounterpartyDto counterpartyDto) {
        Counterparty counterparty = CounterpartyMapper.INSTANCE.toEntity(counterpartyDto);
        counterparty.setIsRemoved(false);
        counterpartyRepository.save(counterparty);
        log.info("All COUNTERPARTY  was created");
    }

    @Override
    @Transactional
    public void update(Long id, CounterpartyDto counterpartyDto) {
        Counterparty fromDto = CounterpartyMapper.INSTANCE.toEntity(counterpartyDto);
        Counterparty newCounterparty = counterpartyRepository.findCounterpartyByIdIfNotRemoved(id);
        newCounterparty.setIsRemoved(false);
        newCounterparty.setStatus(fromDto.getStatus());
        newCounterparty.setGroups(fromDto.getGroups());
        newCounterparty.setTelephoneNumber(fromDto.getTelephoneNumber());
        newCounterparty.setFax(fromDto.getFax());
        newCounterparty.setEmailAddress(fromDto.getEmailAddress());
        newCounterparty.setAddress(fromDto.getAddress());
        newCounterparty.setComment(fromDto.getComment());
        newCounterparty.setCode(fromDto.getCode());
        newCounterparty.setExternalCode(fromDto.getExternalCode());
        counterpartyRepository.save(newCounterparty);
        log.info("All COUNTERPARTY was updated");
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        counterpartyRepository.markCounterpartyAsRemoved(id);
        log.info("COUNTERPARTY with id = {} was marked as removed", id);
    }


}