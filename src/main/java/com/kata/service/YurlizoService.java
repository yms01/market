package com.kata.service;


import com.kata.model.Yurlizo;

import java.util.Optional;


public interface YurlizoService {

    Yurlizo createYurlizo(Yurlizo yurlizo);

    Optional<Yurlizo> getYurlizoById(long yurlizoId);


    Iterable<Yurlizo> getYurlizaByName(String name);

    Iterable<Yurlizo> getYurlizaByInn(int inn);

    Iterable<Yurlizo> getYurliza();

    void updateYurlizo(Long yurlizoId, Yurlizo yurlizo);

    void deleteYurlizoById(long yurlizoId);

    void deleteYurliza();


}
