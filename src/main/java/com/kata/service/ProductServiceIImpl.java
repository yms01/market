package com.kata.service;

import com.kata.dto.product.ProductRequestDto;
import com.kata.dto.product.ProductResponseDto;
import com.kata.mapper.ProductMapper;
import com.kata.model.Product;
import com.kata.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProductServiceIImpl implements ProductService {

    private final ProductRepository productRepository;

    private static final String PRODUCT_WITH_ID_NOT_FOUND = "Товар с id = %d не найден";
    private static final String PRODUCT_ALREADY_EXISTS = "Такой товар уже существует";

    @Override
    public List<ProductResponseDto> findAll() {
        List<ProductResponseDto> productResponseDtoList = ProductMapper.INSTANCE.toDtoList(productRepository.findAllTypes());
        log.info("All products, products-services and products-sets was found");
        return productResponseDtoList;
    }

    @Override
    public List<ProductResponseDto> findAllProducts() {
        List<ProductResponseDto> productResponseDtoList = ProductMapper.INSTANCE.toDtoList(productRepository.findAllProducts());
        log.info("All products was found");
        return productResponseDtoList;
    }

    @Override
    public List<ProductResponseDto> findAllServices() {
        List<ProductResponseDto> productResponseDtoList = ProductMapper.INSTANCE.toDtoList(productRepository.findAllServices());
        log.info("All products-services was found");
        return productResponseDtoList;
    }

    @Override
    public List<ProductResponseDto> findAllSets() {
        List<ProductResponseDto> productResponseDtoList = ProductMapper.INSTANCE.toDtoList(productRepository.findAllSets());
        log.info("All products-sets was found");
        return productResponseDtoList;
    }

    @Override
    public ProductResponseDto findById(Long id) {
        ProductResponseDto productResponseDto = ProductMapper.INSTANCE.toDto(productRepository.findProductById(id).orElse(null));
        if (productResponseDto == null) {
            log.warn("Product with id = {} not found", id);
            throw new EntityNotFoundException(String.format(PRODUCT_WITH_ID_NOT_FOUND, id));
        }
        log.info("Product with id = {} was found", id);
        return productResponseDto;
    }

    @Transactional
    @Override
    public void save(ProductRequestDto productRequestDto) {
        Product product = ProductMapper.INSTANCE.toEntity(productRequestDto);
        product.setLastEdited(LocalDateTime.now());
        product.setIsRemoved(false);
        productRepository.save(product);
        log.info("New product was saved: {}", product);
    }

    @Transactional
    @Override
    public void update(Long id, ProductRequestDto productRequestDto) {
        Product product = ProductMapper.INSTANCE.toEntity(productRequestDto);
        if (productRepository.findById(id).orElse(null) == null) {
            log.warn("Vat with id = {} not found", id);
            throw new EntityNotFoundException(String.format(PRODUCT_WITH_ID_NOT_FOUND, id));
        }
        product.setId(id);
        product.setLastEdited(LocalDateTime.now());
        productRepository.save(product);
        log.info("Product was updated: {}", product);
    }

    @Transactional
    @Override
    public void delete(Long id) {
        if (!productRepository.existsById(id)) {
            log.warn("Product with id = {} not found", id);
            throw new EntityNotFoundException(String.format(PRODUCT_WITH_ID_NOT_FOUND, id));
        }
        productRepository.markAsRemoved(id);
        log.info("Product with id = {} was marked as removed", id);
    }
}
