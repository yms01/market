package com.kata.service;

import com.kata.dto.VatRequestDto;
import com.kata.dto.VatResponseDto;

import java.util.List;

public interface VatService {

    List<VatResponseDto> findAll();

    VatResponseDto findById(Long id);

    void save(VatRequestDto vatResponseDto);

    void update(Long id, VatRequestDto vatResponseDto);

    void deleteById(Long id);
}
