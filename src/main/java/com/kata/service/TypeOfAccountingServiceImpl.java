package com.kata.service;

import com.kata.dto.TypeOfAccountingDto;
import com.kata.mapper.TypeOfAccountingMapper;
import com.kata.model.TypeOfAccounting;
import com.kata.repository.TypeOfAccountingRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


    @Service
    @Slf4j
    @RequiredArgsConstructor
    public class TypeOfAccountingServiceImpl implements TypeOfAccountingService {

        private final TypeOfAccountingRepository typeOfAccountingRepository;

        @Override
        public List<TypeOfAccountingDto> findAll() {
            log.info("TYPE OF ACCOUNTING  was found");
            return TypeOfAccountingMapper.INSTANCE.toDtoList(typeOfAccountingRepository.findAllNotRemoved());
        }

        @Override
        public TypeOfAccountingDto findById(Long id) {
            log.info("TYPE OF ACCOUNTING with id = {} was found", id);
            return TypeOfAccountingMapper.INSTANCE.toDto(typeOfAccountingRepository.findTypeOfAccountingByIdIfNotRemoved(id));
        }

        @Override
        @Transactional
        public void create(TypeOfAccountingDto counterpartyDto) {
            TypeOfAccounting typeOfAccounting = TypeOfAccountingMapper.INSTANCE.toEntity(counterpartyDto);
            typeOfAccounting.setRemoved(false);
            typeOfAccountingRepository.save(typeOfAccounting);
            log.info("All TYPE OF ACCOUNTING  was created");
        }

        @Override
        @Transactional
        public void update(Long id, TypeOfAccountingDto typeOfAccountingDto) {
            TypeOfAccounting fromDto = TypeOfAccountingMapper.INSTANCE.toEntity(typeOfAccountingDto);
            TypeOfAccounting newTypeOfAccounting = typeOfAccountingRepository.findTypeOfAccountingByIdIfNotRemoved(id);
            newTypeOfAccounting.setRemoved(false);
            newTypeOfAccounting.setName(fromDto.getName());
            typeOfAccountingRepository.save(newTypeOfAccounting);
            log.info("All TYPE OF ACCOUNTING was updated");
        }

        @Override
        @Transactional
        public void deleteById(Long id) {
            typeOfAccountingRepository.markTypeOfAccountingAsRemoved(id);
            log.info("TYPE OF ACCOUNTING with id = {} was marked as removed", id);
        }
    }

