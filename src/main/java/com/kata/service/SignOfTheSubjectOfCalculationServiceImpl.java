package com.kata.service;

import com.kata.model.SignOfTheSubjectOfCalculation;
import com.kata.repository.SignOfTheSubjectOfCalculationRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;


@Service
@Slf4j
public class SignOfTheSubjectOfCalculationServiceImpl implements SignOfTheSubjectOfCalculationService {

    final private SignOfTheSubjectOfCalculationRepository signOfTheSubjectOfCalculationRepository;



    @Autowired
    public SignOfTheSubjectOfCalculationServiceImpl(SignOfTheSubjectOfCalculationRepository
                                                            signOfTheSubjectOfCalculationRepository) {
        this.signOfTheSubjectOfCalculationRepository = signOfTheSubjectOfCalculationRepository;
    }

    @Override
    public SignOfTheSubjectOfCalculation createSignOfTheSubjectOfCalculation(
            SignOfTheSubjectOfCalculation signOfTheSubjectOfCalculation) {
        return signOfTheSubjectOfCalculationRepository.save(signOfTheSubjectOfCalculation);
    }


    @Override
    public Optional<SignOfTheSubjectOfCalculation>
    getSignOfTheSubjectOfCalculationById(Long id) {
        return signOfTheSubjectOfCalculationRepository.findById(id);
    }

    @Override
    public Iterable<SignOfTheSubjectOfCalculation>
    getSignOfTheSubjectOfCalculationByArtikul(int artikul) {

        return signOfTheSubjectOfCalculationRepository.findAllByArtikul(artikul);
    }

    @Override
    public Iterable<SignOfTheSubjectOfCalculation> getSignOfTheSubjectOfCalculationByName(
            String name) {

        return signOfTheSubjectOfCalculationRepository.findAllByName(name);
    }

    @Override
    public Iterable<SignOfTheSubjectOfCalculation> getSignOfTheSubjectOfCalculations() {

        return signOfTheSubjectOfCalculationRepository.findAll();
    }

    @Override
    public void updateSignOfTheSubjectOfCalculation(
            Long id, SignOfTheSubjectOfCalculation signOfTheSubjectOfCalculation) {

        signOfTheSubjectOfCalculationRepository.findById(id).ifPresent(
                dbSignOfTheSubjectOfCalculation -> {
                    dbSignOfTheSubjectOfCalculation.setName(signOfTheSubjectOfCalculation.getName());
                    dbSignOfTheSubjectOfCalculation.setArtikul(signOfTheSubjectOfCalculation.getArtikul());
                    dbSignOfTheSubjectOfCalculation.setCode(
                            signOfTheSubjectOfCalculation.getCode());
                    dbSignOfTheSubjectOfCalculation.setCode(signOfTheSubjectOfCalculation.getCode());
                    dbSignOfTheSubjectOfCalculation.setVneshcode(
                            signOfTheSubjectOfCalculation.getVneshcode());

                    signOfTheSubjectOfCalculationRepository.save(dbSignOfTheSubjectOfCalculation);
                });
    }

    @Override
    @Transactional
    public void deleteSignOfTheSubjectOfCalculationById(Long id) {
        signOfTheSubjectOfCalculationRepository.markSignOfTheSubjectOfCalculationAsRemoved(id);
    }


}
