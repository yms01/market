package com.kata.service;

import com.kata.dto.PackagingDTO;
import java.util.List;

public interface PackagingService {

    List<PackagingDTO> index();

    PackagingDTO showPackaging(int id);

    void savePackaging(PackagingDTO packagingDTO);

    void updatePackaging(int id, PackagingDTO packagingDTO);

    void deletePackaging(int id);
}
