package com.kata.service.warehouse;

import com.kata.dto.warehouse.WarehouseRequestDto;
import com.kata.model.warehouse.Zone;

import java.util.Set;

public interface ZoneService {
    Set<Zone> getZonesForUpdate (Long id, WarehouseRequestDto dto);

    Set<Zone> getZonesForSave(WarehouseRequestDto dto);
}
