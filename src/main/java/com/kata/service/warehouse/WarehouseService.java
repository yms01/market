package com.kata.service.warehouse;

import com.kata.dto.warehouse.WarehouseRequestDto;
import com.kata.dto.warehouse.WarehouseResponseDto;
import com.kata.model.warehouse.Warehouse;

import java.util.List;

public interface WarehouseService {
    List<WarehouseResponseDto> findAll();

    WarehouseResponseDto findById(Long id);

    Warehouse save(WarehouseRequestDto dto);

    void update(Long id, WarehouseRequestDto dto);

    void deleteById(Long id);
}
