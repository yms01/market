package com.kata.service.warehouse;

import com.kata.dto.warehouse.WarehouseRequestDto;
import com.kata.mapper.warehouse.ZoneMapper;
import com.kata.model.warehouse.Warehouse;
import com.kata.model.warehouse.Zone;
import com.kata.repository.warehouse.WarehouseRepository;
import com.kata.repository.warehouse.ZoneRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Set;

import static java.util.Objects.nonNull;

@Service
@AllArgsConstructor
public class ZoneServiceImpl implements ZoneService {
    private final ZoneMapper zoneMapper;
    private final ZoneRepository zoneRepository;
    private final WarehouseRepository warehouseRepository;

    @Override
    @Transactional
    public Set<Zone> getZonesForUpdate(Long id, WarehouseRequestDto dto) {
        Warehouse warehouse = warehouseRepository.getById(id);

        if (nonNull(dto.getZones())) {

            Set<Zone> zones = zoneMapper.toZoneSet(dto.getZones());

            if (!(warehouse.getZones().isEmpty())) {
                zones.forEach(zone -> {
                    if (!warehouse.getZones().contains(zone)) {
                        zone.setTotalCells(0);
                        zone.setFreeCells(0);
                        zone.setOccupiedCells(0);
                    }
                    zoneRepository.saveAll(zones);
                });
            } else {
                zones.forEach(zone -> {
                    zone.setTotalCells(0);
                    zone.setFreeCells(0);
                    zone.setOccupiedCells(0);
                });
                zoneRepository.saveAll(zones);
            }
        }
        return warehouse.getZones();
    }

    @Override
    @Transactional
    public Set<Zone> getZonesForSave(WarehouseRequestDto dto) {
        if (!(dto.getZones().isEmpty())) {
            Set<Zone> zones = zoneMapper.toZoneSet(dto.getZones());

            zones.forEach(zone -> {
                zone.setTotalCells(0);
                zone.setFreeCells(0);
                zone.setOccupiedCells(0);
            });
            zoneRepository.saveAll(zones);

            return zones;
        }
        return null;
    }
}
