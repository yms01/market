package com.kata.service.warehouse;

import com.kata.dto.warehouse.CellRequestDto;
import com.kata.model.warehouse.Cell;

import java.util.Set;

public interface CellService {
    Set<Cell> getFilledCells(Set<CellRequestDto> cellDtoSet);
}
