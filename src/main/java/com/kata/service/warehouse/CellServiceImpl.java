package com.kata.service.warehouse;

import com.kata.dto.warehouse.CellRequestDto;
import com.kata.mapper.warehouse.CellMapper;
import com.kata.model.warehouse.Cell;
import com.kata.model.warehouse.Zone;
import com.kata.repository.warehouse.CellRepository;
import com.kata.repository.warehouse.ZoneRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import static java.util.Objects.isNull;

@Service
@AllArgsConstructor
public class CellServiceImpl implements CellService {

    private final CellMapper cellMapper;
    private final ZoneRepository zoneRepository;
    private final CellRepository cellRepository;

    @Override
    @Transactional
    public Set<Cell> getFilledCells(Set<CellRequestDto> cellDtoSet) {
        Set<Cell> cells = new HashSet<>();

        if (!isNull(cellDtoSet)) {
            cells = cellMapper.toCellSet(cellDtoSet);
            Iterator<Cell> cellIterator = cells.iterator();
            Iterator<CellRequestDto> dtoIterator = cellDtoSet.iterator();

            while (cellIterator.hasNext() && dtoIterator.hasNext()) {
                Cell cell = cellIterator.next();
                CellRequestDto dto = dtoIterator.next();
                Zone zone = zoneRepository.findById(dto.getZoneId()).orElse(null);

                cell.setZone(zone);
                cell.setFree(true);

                cellRepository.save(cell);

                if (!isNull(zone)) {
                    zone.setTotalCells(zone.getTotalCells() + 1);
                    zone.setFreeCells(zone.getFreeCells() + 1);
                }
            }
        }
        return cells;
    }


}

