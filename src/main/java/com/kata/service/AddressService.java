package com.kata.service;

import com.kata.dto.AddressRequestDto;
import com.kata.dto.AddressResponseDto;

import java.util.List;

public interface AddressService {

    List<AddressResponseDto> findAll();

    AddressResponseDto findById(Long id);

    void save(AddressRequestDto addressRequestDto);

    void update(Long id, AddressRequestDto addressRequestDto);

    void delete(Long id);
}