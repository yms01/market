package com.kata.service;

import com.kata.dto.VatRequestDto;
import com.kata.dto.VatResponseDto;
import com.kata.mapper.VatMapper;
import com.kata.model.Vat;
import com.kata.repository.VatRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class VatServiceImpl implements VatService {

    private final VatRepository vatRepository;

    private static final String VAT_WITH_ID_NOT_FOUND = "НДС с id = %d не найден";
    private static final String VAT_WITH_RATE_ALREADY_EXISTS = "НДС cо ставкой = %d уже существует";

    @Override
    public List<VatResponseDto> findAll() {
        log.info("All VAT rates was found");
        return VatMapper.INSTANCE.toDtoList(vatRepository.findAll());
    }

    @Override
    public VatResponseDto findById(Long id) {
        VatResponseDto vatResponseDto = VatMapper.INSTANCE.toDto(vatRepository.findById(id).orElse(null));
        if (vatResponseDto == null) {
            log.warn("VAT with id = {} not found", id);
            throw new EntityNotFoundException(String.format(VAT_WITH_ID_NOT_FOUND, id));
        }
        log.info("VAT rate with id = {} was found", id);
        return vatResponseDto;
    }

    @Override
    @Transactional
    public void save(VatRequestDto vatRequestDto) {
        Vat vat = VatMapper.INSTANCE.toEntity(vatRequestDto);
        if (Boolean.TRUE.equals(vatRepository.existsVatByRate(vat.getRate()))) {
            log.warn("Vat with rate = {} already exists", vat.getRate());
            throw new EntityExistsException(String.format(VAT_WITH_RATE_ALREADY_EXISTS, vat.getRate()));
        }
        vat.setLastEdited(LocalDateTime.now());
        vat.setIsSystem(false);
        vat.setIsRemoved(false);
        vatRepository.save(vat);
        log.info("VAT rate was saved: {}", vat);
    }

    @Override
    @Transactional
    public void update(Long id, VatRequestDto vatRequestDto) {
        Vat vat = VatMapper.INSTANCE.toEntity(vatRequestDto);
        if (vatRepository.findById(id).orElse(null) == null) {
            log.warn("Vat with id = {} not found", id);
            throw new EntityNotFoundException(String.format(VAT_WITH_ID_NOT_FOUND, id));
        }
        if (vatRepository.exists(Example.of(vat))) {
            log.warn("Vat already exists: {}", vat);
            throw new EntityExistsException(String.format(VAT_WITH_RATE_ALREADY_EXISTS, vat.getRate()));
        }
        vat.setId(id);
        vat.setLastEdited(LocalDateTime.now());
        vatRepository.save(vat);
        log.info("VAT rate was updated: {}", vat);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        if (!vatRepository.existsById(id)) {
            log.warn("VAT with id = {} not found", id);
            throw new EntityNotFoundException(String.format(VAT_WITH_ID_NOT_FOUND, id));
        }
        vatRepository.markVatAsRemoved(id);
        log.info("VAT rate with id = {} was marked as removed", id);
    }
}
