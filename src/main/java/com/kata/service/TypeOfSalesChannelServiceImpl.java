package com.kata.service;

import com.kata.model.TypeOfSalesChannel;
import com.kata.repository.TypeOfSalesChannelRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TypeOfSalesChannelServiceImpl implements TypeOfSalesChannelService {
    TypeOfSalesChannelRepository repository;

    @Override
    public List<TypeOfSalesChannel> findAllTypeOfSalesChannel() {
        return repository.findAll();
    }
}
