package com.kata.service;

import com.kata.dto.SalesChannelDTO;
import com.kata.model.SalesChannel;
import com.kata.repository.SalesChannelRepository;
import com.kata.mapper.SalesChannelMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class SalesChannelServiceImpl implements SalesChannelService {
    private final SalesChannelRepository repository;

    @Override
    public List<SalesChannelDTO> findAll() {

        log.info("All Sales Channels was found");
        return SalesChannelMapper.INSTANCE.toDtoList(repository.findAllNotRemoved());
    }

    @Override
    public SalesChannelDTO findById(Long id) {

        log.info("Sales Channel with id = {} was found", id);
        return SalesChannelMapper.INSTANCE.toDto(repository.findSalesChannelByIdIfNotRemoved(id));
    }

    @Override
    @Transactional
    public void save(SalesChannelDTO salesChannelDTO) {
        SalesChannel salesChannel = SalesChannelMapper.INSTANCE.toEntity(salesChannelDTO);

        salesChannel.setIsRemoved(false);
        repository.save(salesChannel);
        log.info("Sales Channel was saved: {}", salesChannel);
    }

    @Override
    @Transactional
    public void update(Long id, SalesChannelDTO salesChannelDTO) {
        SalesChannel fromDto = SalesChannelMapper.INSTANCE.toEntity(salesChannelDTO);
        SalesChannel newSalesChannel = repository.findSalesChannelByIdIfNotRemoved(id);
        newSalesChannel.setName(fromDto.getName());
        newSalesChannel.setType(fromDto.getType());
        newSalesChannel.setDescription(fromDto.getDescription());
        newSalesChannel.setIsRemoved(false);
        log.info("Sales Channel was updated: {}", newSalesChannel);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        repository.markSalesChannelAsRemoved(id);
        log.info("Sales Channel with id = {} was marked as removed", id);
    }
}
