package com.kata.service;

import com.kata.dto.TaxationSystemDTO;

import java.util.List;

public interface TaxationSystemService {

    List<TaxationSystemDTO> findAll();

    TaxationSystemDTO findById(Long id);

    void save(TaxationSystemDTO taxationSystemDTO);

    void update(Long id, TaxationSystemDTO taxationSystemDTO);

    void deleteById(Long id);
}
