package com.kata.service;

import com.kata.dto.CurrencyDTO;
import java.util.List;

public interface CurrencyService {

    List<CurrencyDTO> index();

    CurrencyDTO showCurrency(int id);

    void saveCurrency(CurrencyDTO currencyDTO);

    void updateCurrency(int id, CurrencyDTO currencyDTO);

    void deleteCurrency(int id);
}
