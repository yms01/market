package com.kata.service;

import com.kata.dto.AddressRequestDto;
import com.kata.dto.AddressResponseDto;
import com.kata.mapper.AddressMapper;
import com.kata.model.Address;
import com.kata.repository.AddressRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class AddressServiceImpl implements AddressService {

    private final AddressRepository addressRepository;

    private static final String ADDRESS_WITH_ID_NOT_FOUND = "Адрес с id = %d не найден";
    private static final String ADDRESS_ALREADY_EXISTS = "Такой адрес уже существует";

    @Override
    public List<AddressResponseDto> findAll() {
        List<AddressResponseDto> addressResponseDtoList = AddressMapper.INSTANCE.toDtoList(addressRepository.findAllAddresses());
        log.info("All addresses was found");
        return addressResponseDtoList;
    }

    @Override
    public AddressResponseDto findById(Long id) {
        AddressResponseDto addressResponseDto = AddressMapper.INSTANCE.toDto(addressRepository.findAddressById(id).orElse(null));
        if (addressResponseDto == null) {
            log.warn("Address with id = {} not found", id);
            throw new EntityNotFoundException(String.format(ADDRESS_WITH_ID_NOT_FOUND, id));
        }
        log.info("Address with id = {} was found", id);
        return addressResponseDto;
    }

    @Transactional
    @Override
    public void save(AddressRequestDto addressRequestDto) {
        Address address = AddressMapper.INSTANCE.toEntity(addressRequestDto);
        if (addressRepository.exists(Example.of(address))) {
            log.warn("Address already exists: {}", address);
            throw new EntityExistsException(ADDRESS_ALREADY_EXISTS);
        }
        address.setIsRemoved(false);
        addressRepository.save(address);
        log.info("New address was saved: {}", address);
    }

    @Transactional
    @Override
    public void update(Long id, AddressRequestDto addressRequestDto) {
        Address address = AddressMapper.INSTANCE.toEntity(addressRequestDto);
        if (addressRepository.findById(id).orElse(null) == null) {
            log.warn("Address with id = {} not found", id);
            throw new EntityNotFoundException(String.format(ADDRESS_WITH_ID_NOT_FOUND, id));
        }
        if (addressRepository.exists(Example.of(address))) {
            log.warn("Address already exists: {}", address);
            throw new EntityExistsException(ADDRESS_ALREADY_EXISTS);
        }
        address.setId(id);
        addressRepository.save(address);
        log.info("Address was updated: {}", address);
    }

    @Transactional
    @Override
    public void delete(Long id) {
        if (!addressRepository.existsById(id)) {
            log.warn("Address with id = {} not found", id);
            throw new EntityNotFoundException(String.format(ADDRESS_WITH_ID_NOT_FOUND, id));
        }
        addressRepository.markAddressAsRemoved(id);
        log.info("Address with id = {} was marked as removed", id);
    }

}
