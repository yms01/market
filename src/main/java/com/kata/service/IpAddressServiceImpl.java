package com.kata.service;

import com.kata.model.IpAddress;
import com.kata.repository.IpAddressRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class IpAddressServiceImpl implements IpAddressService{
    private final IpAddressRepository repository;

    @Override
    public List<IpAddress> findAllIpAddresses() {
        return repository.findAll();
    }
}
