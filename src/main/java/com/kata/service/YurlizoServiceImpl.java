package com.kata.service;


import com.kata.model.Yurlizo;
import com.kata.repository.YurlizoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;

import java.util.Optional;


@Service
@Slf4j
public class YurlizoServiceImpl implements YurlizoService {

   final private YurlizoRepository yurlizoRepository;

    @Autowired
    public YurlizoServiceImpl(YurlizoRepository yurlizoRepository) {
        this.yurlizoRepository = yurlizoRepository;
    }

    @Override
    public Yurlizo createYurlizo(Yurlizo course) {
        return yurlizoRepository.save(course);
    }

    @Override
    public Optional<Yurlizo> getYurlizoById(long yurlizoId) {
        return yurlizoRepository.findById(yurlizoId);
    }


    @Override
    public Iterable<Yurlizo> getYurlizaByInn(int inn) {
        return yurlizoRepository.findAllByInn(inn);
    }

    @Override
    public Iterable<Yurlizo> getYurlizaByName(String name) {
        return yurlizoRepository.findAllByName(name);
    }

    @Override
    public Iterable<Yurlizo> getYurliza() {
        return yurlizoRepository.findAll();
    }

    @Override
    public void updateYurlizo(Long yurlizoId, Yurlizo yurlizo) {

        yurlizoRepository.findById(yurlizoId).ifPresent(dbYurlizo -> {
            dbYurlizo.setName(yurlizo.getName());
            dbYurlizo.setType(yurlizo.getType());
            dbYurlizo.setYuridichadres(yurlizo.getYuridichadres());
            dbYurlizo.setInn(yurlizo.getInn());
            dbYurlizo.setKpp(yurlizo.getKpp());
            dbYurlizo.setOgrn(yurlizo.getOgrn());
            dbYurlizo.setOkpo(yurlizo.getOkpo());
            yurlizoRepository.save(dbYurlizo);
        });
    }

    @Override
    public void deleteYurliza() {
        yurlizoRepository.deleteAll();
    }

    @Override
    public void deleteYurlizoById(long yurlizoId) {
        yurlizoRepository.deleteById(yurlizoId);
    }




}
