package com.kata.service;

import com.kata.dto.ProductTypeRequestDTO;
import com.kata.dto.ProductTypeResponseDTO;

import java.util.List;

/**
 * @ In the name of Allah, most gracious and most merciful! 02.12.2022
 */
public interface ProductTypeService {
    List<ProductTypeResponseDTO> getAllProductTypes();

    ProductTypeResponseDTO getProductType(Long id);

    void addProductType(ProductTypeRequestDTO productTypeRequestDTO);

    void updateProductType(Long id, ProductTypeRequestDTO productTypeRequestDTO);

    void deleteProductType(Long id);
}