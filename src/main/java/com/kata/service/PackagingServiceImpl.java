package com.kata.service;

import com.kata.dto.PackagingDTO;
import com.kata.mapper.PackagingMapper;
import com.kata.model.Packaging;
import com.kata.repository.PackagingRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
@Transactional
@Slf4j
public class PackagingServiceImpl implements PackagingService {

    private final PackagingRepository packagingRepository;

    @Autowired
    public PackagingServiceImpl(PackagingRepository packagingRepository) {
        this.packagingRepository = packagingRepository;
    }

    public List<PackagingDTO> index() {
        log.info("All Packaging was found");
        return PackagingMapper.INSTANCE.modelsToDTOs(packagingRepository.findAllNotRemoved());
    }

    public PackagingDTO showPackaging(int id) {
        log.info("Packaging with id = {} was found", packagingRepository.getReferenceById(id));
        return PackagingMapper.INSTANCE.modelToDto(packagingRepository.findPackagingByIdIfNotRemoved(id));
    }

    public void savePackaging(PackagingDTO packagingDTO) {
        Packaging packaging = PackagingMapper.INSTANCE.dtoToModel(packagingDTO);
        packaging.setRemoved(false);
        packagingRepository.save(packaging);
        log.info("Packaging was saved: {}", packaging);
    }

    public void updatePackaging(int id, PackagingDTO packagingDTO) {
        Packaging fromDto = PackagingMapper.INSTANCE.dtoToModel(packagingDTO);
        Packaging newPackaging = packagingRepository.findPackagingByIdIfNotRemoved(id);
        newPackaging.setPackagingType(fromDto.getPackagingType());
        packagingRepository.save(newPackaging);
        log.info("Packaging was updated: {}", newPackaging);
    }

    public void deletePackaging(int id) {
        packagingRepository.markPackagingAsRemoved(id);
        log.info("Packaging with id = {} was marked as removed", id);
    }
}