package com.kata.service;

import com.kata.dto.TypeOfAccountingDto;


import java.util.List;

public interface TypeOfAccountingService {

    List<TypeOfAccountingDto> findAll();

    TypeOfAccountingDto findById(Long id);

    void create (TypeOfAccountingDto typeOfAccountingDto);

    void update(Long id, TypeOfAccountingDto typeOfAccountingDto);

    void deleteById(Long id);
}
