package com.kata.service.document;

import com.kata.dto.document.DocumentDTO;
import java.util.List;

public interface DocumentService {

    List<DocumentDTO> getAllDocuments();

    List<DocumentDTO> findAllToBasket();

    DocumentDTO showDocument(Long id);

    void saveDocument(DocumentDTO documentDTO);

    void updateDocument(Long id, DocumentDTO documentDTO);

    void deleteDocument(Long id);

    void recover(Long id);
}
