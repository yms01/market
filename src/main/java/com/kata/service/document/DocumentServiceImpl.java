package com.kata.service.document;


import com.kata.dto.document.DocumentDTO;
import com.kata.mapper.document.DocumentMapper;
import com.kata.model.document.Document;
import com.kata.repository.document.DocumentRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class DocumentServiceImpl implements DocumentService {

    private final DocumentRepository documentRepository;

    @Override
    public List<DocumentDTO> getAllDocuments() {
        log.info("All Documents was found");
        return DocumentMapper.INSTANCE.modelsToDTOs(documentRepository.findAllNotRemoved());
    }

    @Override
    public List<DocumentDTO> findAllToBasket() {
        log.info("All Documents to basket was found");
        return DocumentMapper.INSTANCE.modelsToDTOs(documentRepository.findAllRemovedToBasket());
    }

    @Override
    public DocumentDTO showDocument(Long id) {
        log.info("Document with id = {} was found", documentRepository.getReferenceById(id));
        return DocumentMapper.INSTANCE.modelToDto(documentRepository.findDocumentByIdIfNotRemoved(id));
    }

    @Override
    @Transactional
    public void saveDocument(DocumentDTO documentDTO) {
        Document document = DocumentMapper.INSTANCE.dtoToModel(documentDTO);
        document.setWhenWasCreated(LocalDateTime.now());
        document.setIsRemoved(false);
        documentRepository.save(document);
        log.info("Document was saved: {}", document);
    }

    @Override
    @Transactional
    public void updateDocument(Long id, DocumentDTO documentDTO) {
        Document fromDto = DocumentMapper.INSTANCE.dtoToModel(documentDTO);
        Document newDocument = documentRepository.findDocumentByIdIfNotRemoved(id);
        newDocument.setIsRemoved(false);
        newDocument.setLastEdited(LocalDateTime.now());
        newDocument.setNumber(fromDto.getNumber());
        newDocument.setCurrency(fromDto.getCurrency());
        newDocument.setFromWarehouse(fromDto.getFromWarehouse());
        newDocument.setCounterparty(fromDto.getCounterparty());
        newDocument.setSalesChannel(fromDto.getSalesChannel());
        newDocument.setGeneralAccess(fromDto.isGeneralAccess());
        newDocument.setStatus(fromDto.getStatus());
        newDocument.setComment(fromDto.getComment());
        newDocument.setWhenWasCreated(fromDto.getWhenWasCreated());
        newDocument.setLastEditedBy(fromDto.getLastEditedBy());
        documentRepository.save(newDocument);
        log.info("Document was updated: {}", newDocument);
    }

    @Override
    @Transactional
    public void deleteDocument(Long id) {
        documentRepository.markDocumentAsRemoved(id);
        log.info("Document with id = {} was marked as removed", id);
    }

    @Override
    @Transactional
    public void recover(Long id) {
        documentRepository.markDocumentAsNotRemoved(id);
        log.info("Document with id = {} was marked as not removed", id);
    }
}
