package com.kata.service.document;

import com.kata.dto.document.OrderRequestDto;
import com.kata.dto.document.OrderResponseDto;
import com.kata.mapper.document.OrderMapper;
import com.kata.model.document.Order;
import com.kata.repository.document.OrderRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Slf4j
@Service
@AllArgsConstructor
public class OrderServiceImpl implements OrderService {
    private final OrderRepository repository;
    private final OrderMapper mapper;

    @Override
    public List<OrderResponseDto> findAll() {
        log.info("All orders have been received");

        return mapper.toDtoList(repository.findAllByRemovedFalse());
    }

    @Override
    public OrderResponseDto findById(Long id) {
        log.info("Order with id = {} was received", id);

        return mapper.toDto(repository.getById(id));
    }

    @Override
    @Transactional
    public void save(OrderRequestDto dto) {
        Order order = mapper.toEntity(dto);

        if (isNull(dto.getNumber())) {
            if (nonNull(repository.getMaxNumber())) {
                order.setNumber(repository.getMaxNumber() + 1);
            } else {
                order.setNumber(1L);
            }
        } else {
            order.setNumber(dto.getNumber());
        }

        repository.save(order);

        log.info("Order was created: {}", dto);
    }

    @Override
    @Transactional
    public void update(Long id, OrderRequestDto dto) {
        Order order = mapper.toEntity(dto);
        order.setId(id);
        order.setWhenWasCreated(repository.getById(id).getWhenWasCreated());
        if (isNull(dto.getNumber())) {
            order.setNumber(repository.getById(id).getNumber());
        }

        repository.save(order);

        log.info("Order was updated: {}", dto);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        repository.markAsRemoved(id);

        log.info("Order with id = {} was marked as removed", id);
    }
}
