package com.kata.service;

import com.kata.dto.CounterpartyDto;

import java.util.List;

public interface CounterpartyService {

    List<CounterpartyDto> findAll();

    CounterpartyDto findById(Long id);

    void create(CounterpartyDto counterpartyDto);

    void update(Long id, CounterpartyDto counterpartyDto);

    void deleteById(Long id);
}

