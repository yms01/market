package com.kata.config.db.initilizer;

import com.kata.model.Department;
import com.kata.model.Employee;
import com.kata.model.IpAddress;
import com.kata.model.IpNetwork;
import com.kata.model.Notifications;
import com.kata.model.Role;
import com.kata.repository.DepartmentRepository;
import com.kata.repository.EmployeeRepository;
import com.kata.repository.IpAddressRepository;
import com.kata.repository.IpNetworkRepository;
import com.kata.repository.NotificationsRepository;
import com.kata.repository.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

@Component
@RequiredArgsConstructor
public class EmployeeInitializer implements DataInitializer{
    private final DepartmentRepository departmentRepository;
    private final EmployeeRepository employeeRepository;
    private final IpAddressRepository ipAddressRepository;
    private final IpNetworkRepository ipNetworkRepository;
    private final NotificationsRepository notificationsRepository;
    private final RoleRepository roleRepository;

    private static final List<Department> DEPARTMENT_LIST = new ArrayList<>();
    private static final List<Employee> EMPLOYEE_LIST = new ArrayList<>();
    private static final List<IpAddress> IP_ADDRESS_LIST = new ArrayList<>();
    private static final List<IpNetwork> IP_NETWORK_LIST = new ArrayList<>();
    private static final List<Notifications> NOTIFICATIONS_LIST = new ArrayList<>();
    private static final List<Role> ROLE_LIST = new ArrayList<>();

    private static final Random RANDOM = new Random();

    private static final int COUNT = 5;

    @Override
    public void dataInitialization() {

//        ------------------    Role    ------------------------       //
        Role role1 = Role.builder().name("Администратор").build();
        Role role2 = Role.builder().name("Пользователь").build();
        Role role3 = Role.builder().name("Доступ только к точкам продаж").build();

        ROLE_LIST.addAll(Arrays.asList(role1, role2, role3));

//        ------------------    Department    ------------------------       //
        for (int departmentIndex = 1; departmentIndex <= COUNT; departmentIndex++) {
            DEPARTMENT_LIST.add(Department.builder()
                    .name("Департамент " + departmentIndex)
                    .build());
        }

//        ------------------    Notifications    ------------------------       //
        for (int notificationIndex = 1; notificationIndex <= COUNT; notificationIndex++) {
            Notifications notifications = Notifications.builder().customerOrders(RANDOM.nextBoolean())
                    .customerOrdersEmail(RANDOM.nextBoolean()).customerOrdersTelephone(RANDOM.nextBoolean())
                    .customerAccounts(RANDOM.nextBoolean()).customerAccountsEmail(RANDOM.nextBoolean())
                    .customerAccountsTelephone(RANDOM.nextBoolean()).remnants(RANDOM.nextBoolean())
                    .remnantsEmail(RANDOM.nextBoolean()).remnantsTelephone(RANDOM.nextBoolean())
                    .retailTrade(RANDOM.nextBoolean()).retailTradeEmail(RANDOM.nextBoolean())
                    .retailTradeTelephone(RANDOM.nextBoolean()).tasks(RANDOM.nextBoolean())
                    .tasksEmail(RANDOM.nextBoolean()).tasksTelephone(RANDOM.nextBoolean())
                    .dataExchange(RANDOM.nextBoolean()).dataExchangeEmail(RANDOM.nextBoolean())
                    .dataExchangeTelephone(RANDOM.nextBoolean()).scenariosEmail(RANDOM.nextBoolean())
                    .scenariosTelephone(RANDOM.nextBoolean()).onlineStoresEmail(RANDOM.nextBoolean())
                    .onlineStoresTelephone(RANDOM.nextBoolean()).build();

            NOTIFICATIONS_LIST.add(notifications);
        }

//        ------------------    Ip Address    ------------------------       //
        for (int ipAddressIndex = 1; ipAddressIndex <= COUNT; ipAddressIndex++) {
            IP_ADDRESS_LIST.add(IpAddress.builder()
                    .value("ip Address " + ipAddressIndex)
                    .build());
        }

//        ------------------    Ip Network    ------------------------       //
        for (int ipNetworkIndex = 1; ipNetworkIndex <= COUNT; ipNetworkIndex++) {
            IP_NETWORK_LIST.add(IpNetwork.builder()
                    .value("Ip Network " + ipNetworkIndex)
                    .build());
        }

//        ------------------    Employee    ------------------------       //
        for (int employeeIndex = 1; employeeIndex <= COUNT; employeeIndex++) {
            Set<IpAddress> ipAddresses = new HashSet<>(RANDOM.nextInt(COUNT)+1);
            Set<IpNetwork> ipNetworks= new HashSet<>(RANDOM.nextInt(COUNT)+1);

            for (int ipAddressIndex = 1; ipAddressIndex <= COUNT; ipAddressIndex++) {
                IpAddress ipAddress = IP_ADDRESS_LIST.get(RANDOM.nextInt(IP_ADDRESS_LIST.size()));
                ipAddresses.add(ipAddress);
            }

            for (int ipNetworkIndex = 1; ipNetworkIndex <= COUNT; ipNetworkIndex++) {
                IpNetwork ipNetwork = IP_NETWORK_LIST.get(RANDOM.nextInt(IP_NETWORK_LIST.size()));
                ipNetworks.add(ipNetwork);
            }

            EMPLOYEE_LIST.add(Employee.builder()
                    .firstName("Имя  " + employeeIndex)
                    .middleName("Отчество " + employeeIndex)
                    .lastName("фамилия " + employeeIndex)
                    .telephone("+" + RANDOM.nextLong(10000000000L, 100000000000L))
                    .post("Почта " + employeeIndex)
                    .individualTaxNumber("" + (RANDOM.nextLong(10000000000L, 100000000000L)))
                    .description("Описание работника " + employeeIndex)
                    .login("Логин " + employeeIndex)
                    .email("EMAIL " + employeeIndex)
                    .department(DEPARTMENT_LIST.get(RANDOM.nextInt(DEPARTMENT_LIST.size())))
                    .role(ROLE_LIST.get(RANDOM.nextInt(ROLE_LIST.size())))
                    .notifications(NOTIFICATIONS_LIST.get(RANDOM.nextInt(NOTIFICATIONS_LIST.size())))
                    .isRemoved(false)
                    .accessFromAddresses(new ArrayList<>(ipAddresses))
                    .accessFromNetwork(new ArrayList<>(ipNetworks))
                    .build());
        }

//----------------------------------------------------------------------------------------------//
        roleRepository.saveAll(ROLE_LIST);
        departmentRepository.saveAll(DEPARTMENT_LIST);
        notificationsRepository.saveAll(NOTIFICATIONS_LIST);
        ipAddressRepository.saveAll(IP_ADDRESS_LIST);
        ipNetworkRepository.saveAll(IP_NETWORK_LIST);
        employeeRepository.saveAll(EMPLOYEE_LIST);
    }
}
