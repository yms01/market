package com.kata.config.db.initilizer;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@RequiredArgsConstructor
@ConditionalOnExpression("${market-accounting.dataInitEnabled}")
public class InitializerStarter {
    private final TaxationSystemInitializer taxationSystemInitializer;

    @PostConstruct
    public void dataInitialization() {

        taxationSystemInitializer.dataInitialization();
    }
}
//        1. Employee (+department, role, notifications, ip_address, ip_network)
//        2. Currency
//        3. Unit
//        4. ProductType
//        5. Vat
//        6. Audit
//        7. Yurlizo
//        8. Signof (Признак предмета расчета)
//        9. Packaging
//        10. SalesChannel (+ TypeOfSalesChannel)
//        11. TaxationSystem
//        12. Country
//        13. Address
//        14. Barcode (+ TypeOfBarcode)
//        15. Counterparty
//        16. LegalDetails
//        17. Warehouse (+ Zone, Cell)
//        18. GroupOfProducts
//        19. Product (+ Packaging, TypeOfAccounting, SignOfTheSubjectOfCalculation
//        20. Document (+ Status)
//        21. Invoice (+ Organization, Contract, Project, Task, File)
//        22. Order