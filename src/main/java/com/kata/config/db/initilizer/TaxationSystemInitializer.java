package com.kata.config.db.initilizer;

import com.kata.model.TaxationSystem;
import com.kata.repository.TaxationSystemRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
@RequiredArgsConstructor
public class TaxationSystemInitializer implements DataInitializer{
    private final TaxationSystemRepository taxationSystemRepository;

    private static final List<TaxationSystem> TAXATION_SYSTEM_LIST = new ArrayList<>();

    @Override
    public void dataInitialization() {

//        ------------------    Taxation System    ------------------------       //
        TaxationSystem taxationSystem1 = TaxationSystem.builder().name("Совпадает с ККТ").isRemoved(false).build();
        TaxationSystem taxationSystem2 = TaxationSystem.builder().name("ОСН").isRemoved(false).build();
        TaxationSystem taxationSystem3 = TaxationSystem.builder().name("УСН. Доход").isRemoved(false).build();
        TaxationSystem taxationSystem4 = TaxationSystem.builder().name("УСН.Доход-расход").isRemoved(false).build();
        TaxationSystem taxationSystem5 = TaxationSystem.builder().name("ЕСХН").isRemoved(false).build();
        TaxationSystem taxationSystem6 = TaxationSystem.builder().name("ЕНВД").isRemoved(false).build();
        TaxationSystem taxationSystem7 = TaxationSystem.builder().name("Патент").isRemoved(false).build();

        TAXATION_SYSTEM_LIST.addAll(Arrays.asList(taxationSystem1, taxationSystem2, taxationSystem3,
                taxationSystem4, taxationSystem5, taxationSystem6, taxationSystem7));

//----------------------------------------------------------------------------------------------//
        taxationSystemRepository.saveAll(TAXATION_SYSTEM_LIST);
    }
}
