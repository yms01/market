package com.kata.config.db.initializer;

public interface DataInitializer {

    void dataInitialization();
}
