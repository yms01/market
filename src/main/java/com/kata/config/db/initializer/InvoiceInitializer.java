package com.kata.config.db.initializer;

import com.kata.enums.TypeOfContract;
import com.kata.model.Organization;
import com.kata.model.Product;
import com.kata.model.Project;
import com.kata.model.Status;
import com.kata.model.document.Contract;
import com.kata.model.document.File;
import com.kata.model.document.Invoice;
import com.kata.model.document.Task;
import com.kata.repository.document.ContractRepository;
import com.kata.repository.CounterpartyRepository;
import com.kata.repository.CurrencyRepository;
import com.kata.repository.EmployeeRepository;
import com.kata.repository.document.FileRepository;
import com.kata.repository.OrganizationRepository;
import com.kata.repository.ProductRepository;
import com.kata.repository.ProjectRepository;
import com.kata.repository.SalesChannelRepository;
import com.kata.repository.StatusRepository;
import com.kata.repository.document.TaskRepository;
import com.kata.repository.YurlizoRepository;
import com.kata.repository.document.InvoiceRepository;
import com.kata.repository.warehouse.WarehouseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

@Component
@RequiredArgsConstructor
public class InvoiceInitializer implements DataInitializer{
    private final FileRepository fileRepository;
    private final StatusRepository statusRepository;
    private final ProjectRepository projectRepository;
    private final TaskRepository taskRepository;
    private final ContractRepository contractRepository;
    private final OrganizationRepository organizationRepository;
    private final InvoiceRepository invoiceRepository;
    private final EmployeeRepository employeeRepository;
    private final SalesChannelRepository salesChannelRepository;
    private final WarehouseRepository warehouseRepository;
    private final CounterpartyRepository counterpartyRepository;
    private final ProductRepository productRepository;
    private final CurrencyRepository currencyRepository;
    private final YurlizoRepository yurlizoRepository;

    private static final List<File> FILE_LIST = new ArrayList<>();
    private static final List<Status> STATUS_LIST = new ArrayList<>();
    private static final List<Project> PROJECT_LIST = new ArrayList<>();
    private static final List<Task> TASK_LIST = new ArrayList<>();
    private static final List<Contract> CONTRACT_LIST = new ArrayList<>();
    private static final List<Organization> ORGANIZATION_LIST = new ArrayList<>();
    private static final List<Invoice> INVOICE_LIST = new ArrayList<>();
    private static final List<String> TYPE_OF_CONTRACT_LIST = new ArrayList<>();

    private static final Random RANDOM = new Random();

    private static final int COUNT = 5;

    private static final ZoneOffset ZONE_OFFSET = ZoneOffset.of("+03:00");
    private final LocalDateTime randomTimeWhenWasCreated = LocalDateTime.ofEpochSecond(
            ThreadLocalRandom.current().nextLong(LocalDateTime.now().minusMonths(1).toEpochSecond(ZONE_OFFSET),
            LocalDateTime.now().toEpochSecond(ZONE_OFFSET)), 0, ZONE_OFFSET);
    private final LocalDateTime randomTimeLastEdited = LocalDateTime.ofEpochSecond(ThreadLocalRandom
            .current().nextLong(randomTimeWhenWasCreated.toEpochSecond(ZONE_OFFSET),
            LocalDateTime.now().toEpochSecond(ZONE_OFFSET)), 0, ZONE_OFFSET);

    private final LocalDateTime randomTermOfExecution = LocalDateTime.ofEpochSecond(
            ThreadLocalRandom.current().nextLong(LocalDateTime.now().toEpochSecond(ZONE_OFFSET),
                    LocalDateTime.now().plusMonths(1).toEpochSecond(ZONE_OFFSET)), 0, ZONE_OFFSET);

    @Override
    public void dataInitialization() {

//        ------------------    File    ------------------------       //
        for (int fileIndex = 1; fileIndex <= COUNT; fileIndex++) {
            FILE_LIST.add(File.builder()
                    .name("Файл " + fileIndex)
                    .size(RANDOM.nextLong(9999999))
                    .dateAndTimeAdded(LocalDateTime.now())
                    .editedBy(employeeRepository.getReferenceById(RANDOM.nextLong(employeeRepository.count()) + 1))
                    .addedFile(null)
                    .build());
        }

//        ------------------    Status    ------------------------       //
        for (int statusIndex = 1; statusIndex <= COUNT; statusIndex++) {
            STATUS_LIST.add(Status.builder()
                    .name("Статус " + statusIndex)
                    .color("Цвет " + statusIndex)
                    .build());
        }

//        ------------------    Project    ------------------------       //
        for (int projectIndex = 1; projectIndex <= COUNT; projectIndex++) {
            PROJECT_LIST.add(Project.builder()
                    .name("Проект " + projectIndex)
                    .code("" + RANDOM.nextInt(100, 1000))
                    .description("Описание " + projectIndex)
                    .build());
        }

//        ------------------    Task    ------------------------       //
        for (int taskIndex = 1; taskIndex <= COUNT; taskIndex++) {
            TASK_LIST.add(Task.builder()
                    .name("Задача " + taskIndex)
                    .completed(RANDOM.nextBoolean())
                    .executor(employeeRepository.getReferenceById(RANDOM.nextLong(employeeRepository.count())))
                    .term(randomTermOfExecution)
                    .document(null)
                    .build());
        }

//        ------------------    Contract    ------------------------       //
        TYPE_OF_CONTRACT_LIST.add(TypeOfContract.COMMISSION.name());
        TYPE_OF_CONTRACT_LIST.add(TypeOfContract.PURCHASE_AND_SALE.name());
        for (int contractIndex = 1; contractIndex <= COUNT; contractIndex++) {
            CONTRACT_LIST.add(Contract.builder()
                    .organization(yurlizoRepository.getReferenceById(RANDOM.nextLong(yurlizoRepository.count())))
                    .typeOfContract(TypeOfContract.valueOf(TYPE_OF_CONTRACT_LIST.get(RANDOM.nextInt(2))))
                    .code(RANDOM.nextInt(100, 1000))
                    .contractAmount(RANDOM.nextDouble(1000, 1000000))
                    .award(0.F)
//                    .seriesAndNumber(RANDOM.nextInt(10, 100) + " № " + RANDOM.nextInt(100000, 1000000))
                    .build());
        }

//        ------------------    Organization    ------------------------       //
        for (int organizationIndex = 1; organizationIndex <= COUNT; organizationIndex++) {
            ORGANIZATION_LIST.add(Organization.builder()
                    .name("Организация " + organizationIndex)
                    .build());
        }

//        ------------------    Invoice    ------------------------       //
        for (int invoiceIndex = 1; invoiceIndex <= COUNT; invoiceIndex++) {
            Invoice invoice = new Invoice();
            Set<Product> products = new HashSet<>(RANDOM.nextInt(COUNT)+1);
            Set<Task> tasks = new HashSet<>(RANDOM.nextInt(COUNT)+1);
            Set<File> files = new HashSet<>(RANDOM.nextInt(COUNT)+1);

            for (int productIndex = 1; productIndex <= products.size(); productIndex++) {
                Product product = productRepository.getReferenceById(RANDOM.nextLong(productRepository.count()));
                products.add(product);
            }

            for (int taskIndex = 1; taskIndex <= tasks.size(); taskIndex++) {
                Task task = taskRepository.getReferenceById(RANDOM.nextLong(taskRepository.count()));
                tasks.add(task);
            }

            for (int fileIndex = 1; fileIndex <= files.size(); fileIndex++) {
                File file = fileRepository.getReferenceById(RANDOM.nextLong(fileRepository.count()));
                files.add(file);
            }

            Invoice.builder().isConducted(RANDOM.nextBoolean())
                    .isItBuying(RANDOM.nextBoolean())
                    .paidFor(0)
                    .shipped(0)
                    .accepted(0)
                    .isSent(RANDOM.nextBoolean())
                    .isPrinted(RANDOM.nextBoolean())
                    .organization(ORGANIZATION_LIST.get(RANDOM.nextInt(ORGANIZATION_LIST.size())))
                    .planPaymentDate(LocalDate.ofEpochDay(ThreadLocalRandom.current().nextLong(LocalDate
                            .now().toEpochDay(), LocalDate.now().plusMonths(3).toEpochDay())))
                    .incomingNumber("Входящий номер " + invoiceIndex)
                    .dateOfIncomingNumber(randomTimeWhenWasCreated)
                    .warehouse(warehouseRepository.getReferenceById(RANDOM.nextLong(warehouseRepository.count())))
                    .contract(CONTRACT_LIST.get(RANDOM.nextInt(CONTRACT_LIST.size())))
                    .project(PROJECT_LIST.get(RANDOM.nextInt(PROJECT_LIST.size())))
                    .relatedDocuments(null)
                    .productList(new ArrayList<>(products))
                    .vat(RANDOM.nextBoolean())
                    .vatInPrise(true)
                    .interimResult(0.)
                    .total(0.)
                    .taskList(new ArrayList<>(tasks))
                    .fileList(new ArrayList<>(files))
                    .build();
            invoice.setIsRemoved(false);
            invoice.setLastEdited(randomTimeLastEdited);
            invoice.setNumber((long) invoiceIndex);
            invoice.setCurrency(currencyRepository.getReferenceById(RANDOM.nextInt((int) currencyRepository.count())));
            invoice.setFromWarehouse(warehouseRepository.getReferenceById(RANDOM.nextLong(warehouseRepository.count())));
            invoice.setCounterparty(counterpartyRepository.getReferenceById(RANDOM.nextLong(counterpartyRepository.count())));
            invoice.setSalesChannel(salesChannelRepository.getReferenceById(RANDOM.nextLong(salesChannelRepository.count())));
            invoice.setGeneralAccess(RANDOM.nextBoolean());
            invoice.setStatus(STATUS_LIST.get(RANDOM.nextInt(5) + 1));
            invoice.setComment("Комментарий " + invoiceIndex);
            invoice.setWhenWasCreated(randomTimeWhenWasCreated);
            invoice.setLastEditedBy(employeeRepository.getReferenceById(RANDOM.nextLong(employeeRepository.count())));

            INVOICE_LIST.add(invoice);
        }

//----------------------------------------------------------------------------------------------//
        fileRepository.saveAll(FILE_LIST);
        statusRepository.saveAll(STATUS_LIST);
        projectRepository.saveAll(PROJECT_LIST);
        taskRepository.saveAll(TASK_LIST);
        contractRepository.saveAll(CONTRACT_LIST);
        organizationRepository.saveAll(ORGANIZATION_LIST);
        invoiceRepository.saveAll(INVOICE_LIST);
    }
}