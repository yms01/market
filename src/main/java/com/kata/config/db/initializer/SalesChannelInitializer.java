package com.kata.config.db.initializer;

import com.kata.model.SalesChannel;
import com.kata.model.TypeOfSalesChannel;
import com.kata.repository.SalesChannelRepository;
import com.kata.repository.TypeOfSalesChannelRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

@Component
@RequiredArgsConstructor
public class SalesChannelInitializer implements DataInitializer {
    private final SalesChannelRepository salesChannelRepository;
    private final TypeOfSalesChannelRepository typeOfSalesChannelRepository;

    private static final List<TypeOfSalesChannel> TYPE_OF_SALES_CHANNEL_LIST = new ArrayList<>();
    private static final List<SalesChannel> SALES_CHANNEL_LIST = new ArrayList<>();

    private static final Random RANDOM = new Random();

    private static final int SALES_CHANNEL_COUNT = 5;

    @Override
    public void dataInitialization() {

//      -------------  TypeOfSalesChannel   -----------------  //
        TypeOfSalesChannel typeOfSalesChannel1 = TypeOfSalesChannel.builder().name("Мессенджер").build();
        TypeOfSalesChannel typeOfSalesChannel2 = TypeOfSalesChannel.builder().name("Социальная сеть").build();
        TypeOfSalesChannel typeOfSalesChannel3 = TypeOfSalesChannel.builder().name("Маркетплейс").build();
        TypeOfSalesChannel typeOfSalesChannel4 = TypeOfSalesChannel.builder().name("Интернет-магазин").build();
        TypeOfSalesChannel typeOfSalesChannel5 = TypeOfSalesChannel.builder().name("Доска объявлений").build();
        TypeOfSalesChannel typeOfSalesChannel6 = TypeOfSalesChannel.builder().name("Прямые продажи").build();
        TypeOfSalesChannel typeOfSalesChannel7 = TypeOfSalesChannel.builder().name("Другое").build();

        TYPE_OF_SALES_CHANNEL_LIST.addAll(Arrays.asList(typeOfSalesChannel1, typeOfSalesChannel2, typeOfSalesChannel3,
                typeOfSalesChannel4, typeOfSalesChannel5, typeOfSalesChannel6, typeOfSalesChannel7));

//      ----------------  SalesChannel   -----------------  //
        for (int salesChannelIndex = 1; salesChannelIndex <= SALES_CHANNEL_COUNT; salesChannelIndex++) {
            SALES_CHANNEL_LIST.add(SalesChannel.builder()
                    .name("Channel" + salesChannelIndex)
                    .type(TYPE_OF_SALES_CHANNEL_LIST.get(RANDOM.nextInt(TYPE_OF_SALES_CHANNEL_LIST.size())))
                    .description("Sales Channel description " + salesChannelIndex)
                    .isRemoved(false)
                    .build());
        }


//----------------------------------------------------------------------------------------------//
        typeOfSalesChannelRepository.saveAll(TYPE_OF_SALES_CHANNEL_LIST);
        salesChannelRepository.saveAll(SALES_CHANNEL_LIST);
    }
}
