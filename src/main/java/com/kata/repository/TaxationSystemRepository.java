package com.kata.repository;

import com.kata.model.TaxationSystem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TaxationSystemRepository extends JpaRepository<TaxationSystem, Long> {

    @Query("SELECT t FROM TaxationSystem t WHERE t.isRemoved = false")
    List<TaxationSystem> findAllNotRemoved();

    @Query("SELECT t FROM TaxationSystem t WHERE t.id = :id AND t.isRemoved = false")
    TaxationSystem findTaxationSystemByIdIfNotRemoved(Long id);

    @Modifying
    @Query("UPDATE TaxationSystem SET isRemoved = true WHERE id = :id")
    void markTaxationSystemAsRemoved(Long id);
}
