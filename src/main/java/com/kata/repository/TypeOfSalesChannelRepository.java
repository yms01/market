package com.kata.repository;

import com.kata.model.TypeOfSalesChannel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TypeOfSalesChannelRepository  extends JpaRepository<TypeOfSalesChannel, Integer> {
}
