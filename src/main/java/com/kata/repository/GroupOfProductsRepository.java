package com.kata.repository;

import com.kata.model.GroupOfProducts;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface GroupOfProductsRepository extends JpaRepository<GroupOfProducts, Long> {

    @Query("SELECT v FROM GroupOfProducts v WHERE v.isRemoved = false")
    List<GroupOfProducts> findAllNotDeleted();

    @Query("SELECT v FROM GroupOfProducts v WHERE v.idGroup = :idGroup AND v.isRemoved = false")
    GroupOfProducts findGroupByIdIfNotDeleted(Long idGroup);

    @Modifying
    @Query("UPDATE GroupOfProducts SET isRemoved = true WHERE idGroup = :idGroup")
    void markGroupAsDeleted(Long idGroup);


}
