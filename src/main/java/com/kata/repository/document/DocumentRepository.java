package com.kata.repository.document;

import com.kata.model.document.Document;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface DocumentRepository extends JpaRepository<Document, Long> {

    @Query("SELECT sc FROM Document sc WHERE sc.isRemoved = false")
    List<Document> findAllNotRemoved();

    @Query("select d from Document d where d.isRemoved = true and d.lastEdited >= current_date - 7")
    List<Document> findAllRemovedToBasket();

    @Query("SELECT sc FROM Document sc WHERE sc.id = :id AND sc.isRemoved = false")
    Document findDocumentByIdIfNotRemoved(Long id);

    @Modifying
    @Query("UPDATE Document SET isRemoved = true WHERE id = :id")
    void markDocumentAsRemoved(Long id);

    @Modifying
    @Query("UPDATE Document SET isRemoved = false WHERE id = :id")
    void markDocumentAsNotRemoved(Long id);
}
