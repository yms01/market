package com.kata.repository;

import com.kata.model.Unit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UnitRepository extends JpaRepository<Unit, String> {

    Unit findById(Long id);

    @Modifying
    @Query("UPDATE Unit SET isRemoved = true WHERE id=:id")
    void markUnitAsRemoved(Long id);
}
