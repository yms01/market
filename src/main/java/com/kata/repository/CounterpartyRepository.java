package com.kata.repository;

import com.kata.model.Counterparty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CounterpartyRepository extends JpaRepository<Counterparty, Long > {

    @Query("SELECT c FROM Counterparty c WHERE c.isRemoved = false")
    List<Counterparty> findAllNotRemoved();

    @Query("SELECT c FROM Counterparty c WHERE c.id = :id AND c.isRemoved = false")
    Counterparty findCounterpartyByIdIfNotRemoved(Long id);

    @Modifying
    @Query("UPDATE Counterparty c SET c.isRemoved = true WHERE c.id = :id")
    void markCounterpartyAsRemoved(Long id);

}