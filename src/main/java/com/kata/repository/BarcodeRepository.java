package com.kata.repository;

import com.kata.model.Barcode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BarcodeRepository extends JpaRepository <Barcode, Long>  {
    @Query("SELECT v FROM Barcode v WHERE v.isRemoved = false")
    List<Barcode> findAllNotRemoved();

    @Query("SELECT v FROM Barcode v WHERE v.idBarcode = :idBarcode AND v.isRemoved = false")
    Barcode findBarcodeByIdIfNotRemoved(Long idBarcode);

    @Modifying
    @Query("UPDATE Barcode SET isRemoved = true WHERE idBarcode = :idBarcode")
    void markBarcodeAsRemoved(Long idBarcode);

}
