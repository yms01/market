package com.kata.repository;

import com.kata.model.Packaging;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface PackagingRepository extends JpaRepository<Packaging, Integer> {

    @Query("SELECT sc FROM Packaging sc WHERE sc.isRemoved = false")
    List<Packaging> findAllNotRemoved();

    @Query("SELECT sc FROM Packaging sc WHERE sc.id = :id AND sc.isRemoved = false")
    Packaging findPackagingByIdIfNotRemoved(int id);

    @Modifying
    @Query("UPDATE Packaging SET isRemoved = true WHERE id = :id")
    void markPackagingAsRemoved(int id);
}
