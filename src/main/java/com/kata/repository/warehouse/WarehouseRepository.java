package com.kata.repository.warehouse;

import com.kata.model.warehouse.Warehouse;
import lombok.NonNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WarehouseRepository extends JpaRepository<Warehouse, Long> {

    @Query("SELECT w FROM Warehouse w WHERE w.isRemoved = false ")
    List<Warehouse> findAllNotRemoved();

    @Modifying
    @Query("UPDATE Warehouse SET isRemoved = true WHERE id=:id")
    void markAsRemoved(Long id);

    @NonNull Warehouse getById(@NonNull Long id);

}
