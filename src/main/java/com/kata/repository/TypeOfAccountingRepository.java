package com.kata.repository;


import com.kata.model.TypeOfAccounting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TypeOfAccountingRepository extends JpaRepository<TypeOfAccounting, Long> {

    @Query("SELECT t FROM TypeOfAccounting t WHERE t.isRemoved = false")
    List<TypeOfAccounting> findAllNotRemoved();

    @Query("SELECT t FROM TypeOfAccounting t WHERE t.id = :id AND t.isRemoved = false")
    TypeOfAccounting findTypeOfAccountingByIdIfNotRemoved(Long id);

    @Modifying
    @Query("UPDATE TypeOfAccounting SET isRemoved = true WHERE id = :id")
    void markTypeOfAccountingAsRemoved(Long id);

}
