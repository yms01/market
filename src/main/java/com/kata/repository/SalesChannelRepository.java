package com.kata.repository;

import com.kata.model.SalesChannel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SalesChannelRepository extends JpaRepository<SalesChannel, Long> {

    @Query("SELECT sc FROM SalesChannel sc WHERE sc.isRemoved = false")
    List<SalesChannel> findAllNotRemoved();

    @Query("SELECT sc FROM SalesChannel sc WHERE sc.id = :id AND sc.isRemoved = false")
    SalesChannel findSalesChannelByIdIfNotRemoved(Long id);

    @Modifying
    @Query("UPDATE SalesChannel SET isRemoved = true WHERE id = :id")
    void markSalesChannelAsRemoved(Long id);
}
