package com.kata.repository;

import com.kata.model.Currency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface CurrencyRepository extends JpaRepository<Currency, Integer> {

    @Query("SELECT sc FROM Currency sc WHERE sc.isRemoved = false")
    List<Currency> findAllNotRemoved();

    @Query("SELECT sc FROM Currency sc WHERE sc.id = :id AND sc.isRemoved = false")
    Currency findCurrencyByIdIfNotRemoved(int id);

    @Modifying
    @Query("UPDATE Currency SET isRemoved = true WHERE id = :id")
    void markCurrencyAsRemoved(int id);
}