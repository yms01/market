package com.kata.repository;

import com.kata.model.Vat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface VatRepository extends JpaRepository<Vat, Long> {

    Boolean existsVatByRate(Byte rate);

    @Modifying
    @Query("UPDATE Vat SET isRemoved = true WHERE id = :id")
    void markVatAsRemoved(Long id);

}
