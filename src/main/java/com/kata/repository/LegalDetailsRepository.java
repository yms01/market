package com.kata.repository;

import com.kata.model.LegalDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LegalDetailsRepository extends JpaRepository<LegalDetails, Long> {
    @Query("SELECT l FROM LegalDetails l WHERE l.isRemoved = false")
    List<LegalDetails> findAllNotRemoved();

    @Query("SELECT l FROM LegalDetails l WHERE l.id = :id AND l.isRemoved = false")
    LegalDetails findLegalDetailsByIdIfNotRemoved(Long id);

    @Modifying
    @Query("UPDATE LegalDetails SET isRemoved = true WHERE id = :id")
    void markLegalDetailsAsRemoved(Long id);
}
