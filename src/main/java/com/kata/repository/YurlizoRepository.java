package com.kata.repository;


import com.kata.model.Yurlizo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface YurlizoRepository extends JpaRepository <Yurlizo, Long>  {

    Iterable<Yurlizo> findAllByInn(int inn);
    Iterable<Yurlizo> findAllByName(String name);
}
