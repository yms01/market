package com.kata.repository;

import com.kata.model.SignOfTheSubjectOfCalculation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;



@Repository
public interface SignOfTheSubjectOfCalculationRepository extends JpaRepository<

        SignOfTheSubjectOfCalculation, Long> {

        Iterable<SignOfTheSubjectOfCalculation> findAllByArtikul(int artikul);

        Iterable<SignOfTheSubjectOfCalculation> findAllByName(String name);




        @Modifying
        @Query("UPDATE SignOfTheSubjectOfCalculation SET isRemoved = true WHERE id = :id")
        void markSignOfTheSubjectOfCalculationAsRemoved(Long Id);





            }


