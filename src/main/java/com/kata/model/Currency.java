package com.kata.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "currencies")
@Builder
public class Currency {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "is_removed")
    private boolean isRemoved;

    @Column(name = "accounting_currency")
    private boolean accountingCurrency;

    @Column(name = "short_name")
    private String shortName;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "digital_code")
    private int digitalCode;

    @Column(name = "alphabetic_code")
    private String alphabeticCode;

    @Column(name = "rate")
    private double rate;

    @Column(name = "time_when_was_changed")
    private String timeWhenWasChanged;

 }
