package com.kata.model.document;

import com.kata.model.Organization;
import com.kata.model.Product;
import com.kata.model.Project;
import com.kata.model.warehouse.Warehouse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Invoice extends Document{
    private Boolean isConducted;
    private Boolean isItBuying;
    private Integer paidFor;
    private Integer shipped;
    private Integer accepted;
    private Boolean isSent;
    private Boolean isPrinted;
    @ManyToOne
    @JoinColumn
    private Organization organization;
    private LocalDate planPaymentDate;
    private String incomingNumber;
    private LocalDateTime dateOfIncomingNumber;
    @ManyToOne
    @JoinColumn
    private Warehouse warehouse;
    @ManyToOne
    @JoinColumn
    private Contract contract;
    @ManyToOne
    @JoinColumn
    private Project project;
    @ManyToMany(fetch = FetchType.LAZY)
    private List<Document> relatedDocuments;
    @ManyToMany(fetch = FetchType.LAZY)
    private List<Product> productList;
    private Boolean vat;
    private Boolean vatInPrise;
    private Double interimResult;
    private Double total;
    @ManyToMany(fetch = FetchType.LAZY)
    private List<Task> taskList;
    @ManyToMany(fetch = FetchType.LAZY)
    private List<File> fileList;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Invoice invoice = (Invoice) o;
        return Objects.equals(getNumber(), invoice.getNumber()) &&
                Objects.equals(getWhenWasCreated(), invoice.getWhenWasCreated()) &&
                Objects.equals(getStatus(), invoice.getStatus()) &&
                Objects.equals(getIsConducted(), invoice.getIsConducted()) &&
                Objects.equals(getIsItBuying(), invoice.getIsItBuying()) &&
                Objects.equals(getPaidFor(), invoice.getPaidFor()) &&
                Objects.equals(getShipped(), invoice.getShipped()) &&
                Objects.equals(getAccepted(), invoice.getAccepted()) &&
                Objects.equals(getIsSent(), invoice.getIsSent()) &&
                Objects.equals(getIsPrinted(), invoice.getIsPrinted()) &&
                Objects.equals(getOrganization(), invoice.getOrganization()) &&
                Objects.equals(getCounterparty(), invoice.getCounterparty()) &&
                Objects.equals(getPlanPaymentDate(), invoice.getPlanPaymentDate()) &&
                Objects.equals(getIncomingNumber(), invoice.getIncomingNumber()) &&
                Objects.equals(getDateOfIncomingNumber(), invoice.getDateOfIncomingNumber()) &&
                Objects.equals(getSalesChannel(), invoice.getSalesChannel()) &&
                Objects.equals(getWarehouse(), invoice.getWarehouse()) &&
                Objects.equals(getContract(), invoice.getContract()) &&
                Objects.equals(getProject(), invoice.getProject()) &&
                Objects.equals(getRelatedDocuments(), invoice.getRelatedDocuments()) &&
                Objects.equals(getProductList(), invoice.getProductList())&&
                Objects.equals(getVat(), invoice.getVat()) &&
                Objects.equals(getVatInPrise(), invoice.getVatInPrise()) &&
                Objects.equals(getInterimResult(), invoice.getInterimResult()) &&
                Objects.equals(getTotal(), invoice.getTotal()) &&
                Objects.equals(getComment(), invoice.getComment()) &&
                Objects.equals(getTaskList(), invoice.getTaskList()) &&
                Objects.equals(getFileList(), invoice.getFileList());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getNumber(), getWhenWasCreated(), getStatus(), getIsConducted(), getIsItBuying()
                , getPaidFor(), getShipped(), getAccepted(), getIsSent(), getIsPrinted()
                , getOrganization(), getCounterparty(), getPlanPaymentDate(), getIncomingNumber()
                , getDateOfIncomingNumber(), getSalesChannel(), getWarehouse(), getContract()
                , getProject(), getRelatedDocuments(), getProductList(), getVat()
                , getVatInPrise(), getInterimResult(), getTotal(), getComment(), getTaskList(), getFileList());
    }

    @Override
    public String toString() {
        return "Invoice{" +
                "id=" + getId() +
                "number=" + getNumber() +
                ", invoiceTime=" + getWhenWasCreated() +
                ", status=" + getStatus() +
                ", isConducted=" + isConducted +
                ", isItBuying=" + isItBuying +
                ", paidFor=" + paidFor +
                ", shipped=" + shipped +
                ", accepted=" + accepted +
                ", isSent=" + isSent +
                ", isPrinted=" + isPrinted +
                ", organization=" + organization +
                ", counterparty=" + getCounterparty() +
                ", planPaymentDate=" + planPaymentDate +
                ", incomingNumber='" + incomingNumber + '\'' +
                ", dateOfIncomingNumber=" + dateOfIncomingNumber +
                ", salesChannel=" + getSalesChannel() +
                ", warehouse=" + warehouse +
                ", contract=" + contract +
                ", project=" + project +
                ", relatedDocuments=" + relatedDocuments +
                ", productList=" + productList +
                ", vat=" + vat +
                ", vatInPrise=" + vatInPrise +
                ", interimResult=" + interimResult +
                ", total=" + total +
                ", comments='" + getComment() + '\'' +
                ", taskList=" + taskList +
                ", fileList=" + fileList +
                ", isRemoved=" + getIsRemoved() +
                '}';
    }
}
