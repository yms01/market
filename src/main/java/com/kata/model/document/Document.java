package com.kata.model.document;

import com.kata.model.Counterparty;
import com.kata.model.Currency;
import com.kata.model.Employee;
import com.kata.model.SalesChannel;
import com.kata.model.Status;
import com.kata.model.warehouse.Warehouse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Document {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private Boolean isRemoved;

    @UpdateTimestamp
    private LocalDateTime lastEdited;

    private Long number;

    @ManyToOne
    @JoinColumn
    private Currency currency;

    @ManyToOne
    @JoinColumn
    private Warehouse fromWarehouse;

    @ManyToOne
    @JoinColumn
    private Counterparty counterparty;

    @ManyToOne
    @JoinColumn
    private SalesChannel salesChannel;

    private boolean generalAccess;

    @ManyToOne
    @JoinColumn
    private Status status;

    private String comment;

    @CreationTimestamp
    private LocalDateTime whenWasCreated;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    private Employee lastEditedBy;

}
