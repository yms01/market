package com.kata.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "units")
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Unit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String shortName;

    private String fullName;

    private Integer digitalCode;

    private boolean isChangeable;

    private boolean isRemoved;

    @UpdateTimestamp
    private LocalDateTime date;

    @ManyToOne
    @JoinColumn()
    private Employee editedBy;

    public Unit(String shortName, String fullName, int digitalCode,
                boolean isChangeable, LocalDateTime date, Employee editedBy) {
        this.shortName = shortName;
        this.fullName = fullName;
        this.digitalCode = digitalCode;
        this.isChangeable = isChangeable;
        this.date = date;
        this.editedBy = editedBy;
    }

    @Override
    public String toString() {
        return "Unit{" +
                "id=" + id +
                ", shortName='" + shortName + '\'' +
                ", fullName='" + fullName + '\'' +
                ", digitalCode=" + digitalCode +
                ", isChangeable=" + isChangeable +
                ", isRemoved=" + isRemoved +
                ", date=" + date +
                ", editedBy=" + editedBy +
                '}';
    }
}
