package com.kata.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Packaging {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String packagingType;

    private boolean isRemoved;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Packaging packaging)) return false;
        return isRemoved() == packaging.isRemoved() &&
                Objects.equals(getPackagingType(), packaging.getPackagingType());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPackagingType(), isRemoved());
    }
}
