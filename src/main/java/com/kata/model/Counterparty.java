package com.kata.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Counterparty {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "status")
    private String status;
    @Column(name = "groups")
    private String groups;
    @Column(name = "telephoneNumber")
    private Long telephoneNumber;
    @Column(name = "fax")
    private Long fax;
    @Column(name = "emailAddress")
    private String emailAddress;

    @OneToOne
    @JoinColumn
    private Address address;
    @Column(name = "comment")
    private String comment;
    @Column(name = "code")
    private Long code;
    @Column(name = "externalCode")
    private Long externalCode;
    @Column(name = "isRemoved")
    private Boolean isRemoved;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Counterparty that = (Counterparty) o;
        return Objects.equals(status, that.status) &&
                Objects.equals(groups, that.groups) &&
                Objects.equals(telephoneNumber, that.telephoneNumber) &&
                Objects.equals(fax, that.fax) &&
                Objects.equals(emailAddress, that.emailAddress) &&
                Objects.equals(address, that.address) &&
                Objects.equals(comment, that.comment) &&
                Objects.equals(code, that.code) &&
                Objects.equals(externalCode, that.externalCode) &&
                Objects.equals(isRemoved, that.isRemoved);
    }

    @Override
    public int hashCode() {
        return Objects.hash(status, groups, telephoneNumber, fax, emailAddress, address,  comment, code, externalCode, isRemoved);
    }

    @Override
    public String toString() {
        return "Counterpart{" +
                "id=" + id +
                ", status='" + status + '\'' +
                ", groups='" + groups + '\'' +
                ", telephoneNumber=" + telephoneNumber +
                ", fax=" + fax +
                ", emailAddress='" + emailAddress + '\'' +
                ", actualAddress=" + address +
                ", comment='" + comment + '\'' +
                ", code=" + code +
                ", externalCode=" + externalCode +
                ", isRemoved=" + isRemoved +
                '}';
    }
}

