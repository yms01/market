package com.kata.model.warehouse;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Objects;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Zone {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String zoneName;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "zone")
    @JsonManagedReference
    private Set<Cell> cells;

    private Integer totalCells;

    private Integer freeCells;

    private Integer occupiedCells;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Zone zone = (Zone) o;
        return zoneName.equals(zone.zoneName) && Objects.equals(totalCells, zone.totalCells) && Objects.equals(freeCells, zone.freeCells) && Objects.equals(occupiedCells, zone.occupiedCells);
    }

    @Override
    public int hashCode() {
        return Objects.hash(zoneName, totalCells, freeCells, occupiedCells);
    }
}
