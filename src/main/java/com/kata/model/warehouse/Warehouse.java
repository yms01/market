package com.kata.model.warehouse;

import com.kata.model.Address;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Warehouse {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String warehouseName;

    @OneToOne(cascade = CascadeType.ALL)
    private Address address;

    private String comment;

    private Integer code;

    @ManyToOne
    private Warehouse parent;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "parent")
    private List<Warehouse> children;

    private Integer externalCode;

    @OneToMany(cascade = CascadeType.ALL)
    private Set<Zone> zones;

    @OneToMany(cascade = CascadeType.ALL)
    private Set<Cell> cells;

    private boolean isRemoved;
}
