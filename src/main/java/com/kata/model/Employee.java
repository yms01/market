package com.kata.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import java.util.List;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String middleName;
    private String lastName;
    private String telephone;
    private String post;
    private String individualTaxNumber;
    private String description;
    private String login;
    private String email;
    @ManyToOne
    @JoinColumn
    private Department department;
    @ManyToOne
    @JoinColumn
    private Role role;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable
    private List<IpAddress> accessFromAddresses;
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable
    private List<IpNetwork> accessFromNetwork;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn
    private Notifications notifications;
    private Boolean isRemoved;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return Objects.equals(firstName, employee.firstName) &&
                Objects.equals(middleName, employee.middleName) &&
                Objects.equals(lastName, employee.lastName) &&
                Objects.equals(telephone, employee.telephone) &&
                Objects.equals(post, employee.post) &&
                Objects.equals(individualTaxNumber, employee.individualTaxNumber) &&
                Objects.equals(description, employee.description) &&
                Objects.equals(login, employee.login) &&
                Objects.equals(email, employee.email) &&
                Objects.equals(department, employee.department);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, middleName, lastName, telephone, post,
                individualTaxNumber, description, login, email, department);
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", telephone=" + telephone +
                ", post='" + post + '\'' +
                ", individualTaxNumber=" + individualTaxNumber +
                ", description='" + description + '\'' +
                ", login='" + login + '\'' +
                ", email='" + email + '\'' +
                ", department=" + department +
                ", role=" + role +
                ", accessFromAddresses=" + accessFromAddresses +
                ", accessFromNetwork=" + accessFromNetwork +
                ", notifications=" + notifications +
                '}';
    }
}
