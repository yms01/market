package com.kata.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String postalCode;

    @ManyToOne
    @JoinColumn(name = "country_id")
    private Country country;

    private String region;

    private String city;

    private String street;

    private String buildingNumber;

    private String apartmentNumber;

    private String other;

    private String addressComment;

    private Boolean isRemoved;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Address address)) return false;
        return Objects.equals(getPostalCode(), address.getPostalCode()) && Objects.equals(getRegion(), address.getRegion()) && Objects.equals(getCity(), address.getCity()) && Objects.equals(getStreet(), address.getStreet()) && Objects.equals(getBuildingNumber(), address.getBuildingNumber()) && Objects.equals(getApartmentNumber(), address.getApartmentNumber()) && Objects.equals(getOther(), address.getOther()) && Objects.equals(getAddressComment(), address.getAddressComment()) && Objects.equals(getIsRemoved(), address.getIsRemoved());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPostalCode(), getRegion(), getCity(), getStreet(), getBuildingNumber(), getApartmentNumber(), getOther(), getAddressComment(), getIsRemoved());
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", postalCode='" + postalCode + '\'' +
                ", region='" + region + '\'' +
                ", city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", buildingNumber='" + buildingNumber + '\'' +
                ", apartmentNumber='" + apartmentNumber + '\'' +
                ", other='" + other + '\'' +
                ", addressComment='" + addressComment + '\'' +
                ", isRemoved=" + isRemoved +
                '}';
    }
}
