package com.kata.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;

/**
 * @ In the name of Allah, most gracious and most merciful! 16.11.2022
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Builder
public class Country {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;
    @Column(name = "is_system")
    private Boolean isSystem;
    @Column(name = "short_name", nullable = false)
    private String shortName;
    @Column(name = "full_name", nullable = false)
    private String fullName;
    @Column(name = "numeric_code", nullable = false)
    private Integer numericCode;
    @Column(name = "alpha2", nullable = false)
    private String alpha2;
    @Column(name = "alpha3", nullable = false)
    private String alpha3;
    @Column(name = "is_removed")
    private Boolean isRemoved;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Country country = (Country) o;
        return isRemoved == country.isRemoved && Objects.equals(id, country.id) && Objects.equals(shortName, country.shortName) && Objects.equals(fullName, country.fullName) && Objects.equals(numericCode, country.numericCode) && Objects.equals(alpha2, country.alpha2) && Objects.equals(alpha3, country.alpha3);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, shortName, fullName, numericCode, alpha2, alpha3, isRemoved);
    }
}
