package com.kata.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
//@Table
public class GroupOfProducts {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idGroup;

    private boolean isRemoved;

    private String nameGroup;

    private String descriptionGroup;

    private String codeGroup;

    private String externalCodeGroup;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn()
    private Vat vat;  //НДС

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn()
    private TaxationSystem taxationSystem;  //система налогооблажения

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn()
    private Employee lastEditedBy;  //сотрудник

    private LocalDateTime lastEdited;

    @Override
    public String toString() {
        return "Group{" +
                "idGroup=" + idGroup +
                ", isDeleted=" + isRemoved +
                ", nameGroup='" + nameGroup + '\'' +
                ", descriptionGroup='" + descriptionGroup + '\'' +
                ", codeGroup='" + codeGroup + '\'' +
                ", externalCodeGroup='" + externalCodeGroup + '\'' +
                ", vat=" + vat +
                ", taxationSystem=" + taxationSystem +
                ", lastEditedBy=" + lastEditedBy +
                ", lastEdited=" + lastEdited +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GroupOfProducts)) return false;
        GroupOfProducts group = (GroupOfProducts) o;
        return  Objects.equals(isRemoved(), group.isRemoved()) && Objects.equals(getNameGroup(),
                group.getNameGroup()) && Objects.equals(getDescriptionGroup(),
                group.getDescriptionGroup()) && Objects.equals(getCodeGroup(), group.getCodeGroup()) && Objects.equals(getExternalCodeGroup(),
                group.getExternalCodeGroup()) && Objects.equals(getVat(), group.getVat()) && Objects.equals(getTaxationSystem(),
                group.getTaxationSystem()) && Objects.equals(getLastEditedBy(), group.getLastEditedBy()) && Objects.equals(getLastEdited(), group.getLastEdited());
    }

    @Override
    public int hashCode() {
        return Objects.hash(isRemoved(), getNameGroup(), getDescriptionGroup(), getCodeGroup(), getExternalCodeGroup(), getVat(),
                getTaxationSystem(), getLastEditedBy(), getLastEdited());
    }
}
