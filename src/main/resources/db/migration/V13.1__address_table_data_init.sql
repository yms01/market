INSERT INTO address (address_comment, apartment_number, building_number, city, is_removed, other, postal_code, region,
                     street, country_id)
VALUES ('comment 1', '11', '35b', 'city 1', FALSE, 'other 1', 'postal code 1', 'region 1', 'street 1', 1),
       ('comment 2', '22', '25b', 'city 2', FALSE, 'other 2', 'postal code 2', 'region 2', 'street 2', 1),
       ('comment 3', '33', '34', 'city 3', FALSE, 'other 3', 'postal code 3', 'region 3', 'street 3', 1),
       ('comment 4', '44', '6', 'city 4', FALSE, 'other 4', 'postal code 4', 'region 4', 'street 4', 1),
       ('comment 5', '55', '12', 'city 5', FALSE, 'other 55', 'postal code 5', 'region 5', 'street 5', 1);