INSERT INTO public.product_types(
    is_removed, name)
VALUES (false, 'Не маркируется'),
       (false, 'Табачная продукция'),
       (false, 'Обувь'),
       (false, 'Одежда'),
       (false, 'Постельное белье'),
       (false, 'Духи и туалетная вода'),
       (false, 'Фотокамеры и лампы-вспышки'),
       (false, 'Шины и покрышки'),
       (false, 'Молочная продукция'),
       (false, 'Упакованная вода'),
       (false, 'Альтернативная табачная продукция'),
       (false, 'Никотиносодержащая продукция');