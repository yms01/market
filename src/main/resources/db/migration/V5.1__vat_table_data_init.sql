INSERT INTO vat (comment, is_removed, is_system, last_edited, rate, last_edited_by_id)
VALUES (NULL, FALSE, TRUE, NOW(), 0, NULL),
       (NULL, FALSE, TRUE, NOW(), 10, NULL),
       (NULL, FALSE, TRUE, NOW(), 18, NULL),
       (NULL, FALSE, TRUE, NOW(), 20, NULL),
       ('comment 1', FALSE, FALSE, NOW(), 11, 1),
       ('comment 2', FALSE, FALSE, NOW(), 22, 1),
       ('comment 3', FALSE, FALSE, NOW(), 33, 1),
       ('comment 4', FALSE, FALSE, NOW(), 44, 1),
       ('comment 5', FALSE, FALSE, NOW(), 55, 1);