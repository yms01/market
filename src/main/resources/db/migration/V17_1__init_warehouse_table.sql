INSERT INTO zone (free_cells, occupied_cells, total_cells, zone_name)
VALUES (1, 0, 1, 'зона1'),
       (1, 0, 1, 'зона2'),
       (1, 0, 1, 'зона3'),
       (1, 0, 1, 'зона4'),
       (1, 0, 1, 'зона5');

INSERT INTO cell (cell_name, is_free, zone_id)
VALUES ('ячейка1', true, 1),
       ('ячейка2', true, 2),
       ('ячейка3', true, 3),
       ('ячейка4', true, 4),
       ('ячейка5', true, 5);

INSERT INTO warehouse (code, comment, external_code, is_removed, warehouse_name, address_id, parent_id)
VALUES (1, 'comment1', 245, false, 'warehouse1', 1, null),
       (2, 'comment2', 345, false, 'warehouse2', 2, 1),
       (3, 'comment3', 543, false, 'warehouse3', 3, 2),
       (4, 'comment4', 666, false, 'warehouse4', 4, 3),
       (5, 'comment5', 433, false, 'warehouse5', 5, 4);

INSERT INTO warehouse_cells (warehouse_id, cells_id)
VALUES (1, 1),
       (2, 2),
       (3, 3),
       (4, 4),
       (5, 5);

INSERT INTO warehouse_zones (warehouse_id, zones_id)
VALUES (1, 1),
       (2, 2),
       (3, 3),
       (4, 4),
       (5, 5)
