INSERT INTO status (name, color)
VALUES ('ожидание', 'yellow'),
       ('выполнен', 'green'),
       ('на согласовании', 'orange'),
       ('провал', 'rad'),
       ('другой', 'blue');

INSERT INTO project (name, code, description)
VALUES ('Проект века', '2022', 'Здесь будут самые большие продажи'),
       ('Вятский мост', '4747665228', 'Здесь вряд ли получится сорвать куш'),
       ('Путинская яма', '########', 'Всё засекречено'),
       ('Новый русский программатор', 'лд*78ККЕ435', 'За товаром будет приходить робот Вика'),
       ('Новые Брянски', '009', 'Сказали, что будут строить из говна и палок. Нужно найти поставщиков');

INSERT INTO organization (name)
VALUES ('СовНацХоз'),
       ('Зайцы в шлёпках'),
       ('ХОСТ'),
       ('Ядерный паровоз'),
       ('Магазин этого');

INSERT INTO file (name, date_and_time_added, size, edited_by_id)
VALUES ('Всем на фото', NOW(), 566658, 1),
       ('Правила удержания удочки', NOW(), 4445, 1),
       ('Как вести себя на корпоративе', NOW(), 78450, 1),
       ('Какой-то документ', NOW(), 550009, 1),
       ('комната из FloorPlane', NOW(), 333500, 1);



INSERT INTO task (name, completed, executor_id, term, document_id)
VALUES ('узнать новости', FALSE, 1, NOW(), 1),
       ('посидеть подумать', FALSE, 2, NOW(), 2),
       ('пойти рассказать всем', FALSE, 3, NOW(), 3),
       ('взять на заметку по питихатке', FALSE, 4, NOW(), 4),
       ('после завершения - обмыть', FALSE, 5, NOW(), 5);

INSERT INTO contract (id, organization_id, type_of_contract, code, contract_amount, award)
VALUES (nextval('hibernate_sequence'), 1, 'PURCHASE_AND_SALE', 388, 388000, 100),
       (nextval('hibernate_sequence'), 2, 'COMMISSION', 111, 84600, 100),
       (nextval('hibernate_sequence'), 3, 'PURCHASE_AND_SALE', 466, 34000, 100),
       (nextval('hibernate_sequence'), 4, 'PURCHASE_AND_SALE', 944, 570000, 100),
       (nextval('hibernate_sequence'), 5, 'COMMISSION', 565, 55000, 100);



INSERT INTO invoice (id, number, currency_id, from_warehouse_id, counterparty_id, sales_channel_id, general_access,
                     status_id, comment, last_edited_by_id, is_conducted, is_it_buying, paid_for, shipped, accepted,
                     organization_id, plan_payment_date, incoming_number, date_of_incoming_number, warehouse_id, contract_id,
                     project_id, vat, vat_in_prise, interim_result, total)
VALUES (nextval('hibernate_sequence'), 1, 1, 1, 1, 1, true, 1, 'Всё будет хорошо', 1, true, FALSE, 0, 0, 0, 1, null, '1', NOW(), 1,
           1, 1, FALSE, FALSE, 2000, 2035),
       (nextval('hibernate_sequence'), 2, 2, 2, 2, 2, true, 2, 'А может даже лучше', 2, true, FALSE, 3, 2, 3, 2, null, '2', NOW(), 2,
           2, 2, FALSE, FALSE, 344, 555),
       (nextval('hibernate_sequence'), 3, 3, 3, 3, 3, true, 3, 'Весь мир у ног твоих', 3, true, FALSE, 3, 45, 4, 3, null, '3', NOW(), 3,
           3, 3, FALSE, FALSE, 55422, 55490),
       (nextval('hibernate_sequence'), 4, 4, 4, 4, 4, true, 4, 'Иди и бери', 4, true, FALSE, 0, 0, 0, 4, null, '4', NOW(), 4,
           4, 4, FALSE, FALSE, 34, 35),
       (nextval('hibernate_sequence'), 5, 5, 5, 5, 5, true, 5, 'Всё будет хорошо', 5, true, FALSE, 0, 0, 0, 5, null, '5', NOW(), 5,
           5, 5, FALSE, FALSE, 5, 5);

INSERT INTO invoice_file_list (Invoice_id, file_list_id)
VALUES (1, 1),(1, 2),(1, 3),(2, 1),(2, 4),(2, 5),(3, 2),(3, 5),(4, 1),(4, 2),(4, 3),(4, 4),(5, 1),(5, 3),(5, 4),(5, 5);

INSERT INTO invoice_product_list (Invoice_id, product_list_id)
VALUES (1, 1),(1, 2),(1, 3),(2, 1),(2, 4),(2, 5),(3, 2),(3, 5),(4, 1),(4, 2),(4, 3),(4, 4),(5, 1),(5, 3),(5, 4),(5, 5);

INSERT INTO invoice_related_documents (Invoice_id, related_documents_id)
VALUES (1, 6),(1, 7),(1, 8),(2, 4),(2, 9),(2, 10),(3, 7),(3, 10),(4, 6),(4, 7),(4, 8),(4, 9),(5, 1),(5, 8),(5, 9),(5, 10);

INSERT INTO invoice_task_list (Invoice_id, task_list_id)
VALUES (1, 1),(1, 2),(1, 3),(2, 1),(2, 4),(2, 5),(3, 2),(3, 5),(4, 1),(4, 2),(4, 3),(4, 4),(5, 1),(5, 3),(5, 4),(5, 5);
