INSERT INTO department (name)
VALUES ('Основной'),
       ('Дел людских'),
       ('Взаимодействия с поставщиками'),
       ('Корпоративов'),
       ('Всяких нужных людей');

INSERT INTO role (name)
VALUES ('Администратор'),
       ('Пользователь'),
       ('Доступ только к точкам продаж');

INSERT INTO notifications (customer_orders, customer_orders_email, customer_orders_telephone, customer_accounts,
                           customer_accounts_email, customer_accounts_telephone, remnants, remnants_email, remnants_telephone,
                           retail_trade, retail_trade_email, retail_trade_telephone, tasks, tasks_email, tasks_telephone,
                           data_exchange, data_exchange_email, data_exchange_telephone, scenarios_email, scenarios_telephone,
                           online_stores_email, online_stores_telephone)
VALUES (TRUE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE,
            TRUE, FALSE, FALSE, FALSE, TRUE, FALSE),
       (TRUE, FALSE, FALSE, TRUE, FALSE, FALSE, FALSE, TRUE, FALSE, FALSE, TRUE, TRUE, FALSE, TRUE, FALSE, FALSE,
            TRUE, FALSE, FALSE, FALSE, TRUE, FALSE),
       (TRUE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE,
            TRUE, FALSE, FALSE, FALSE, TRUE, FALSE),
       (TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE,
            TRUE, TRUE, TRUE, TRUE, TRUE, TRUE),
       (TRUE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, TRUE, TRUE, FALSE, TRUE, FALSE, FALSE, TRUE, FALSE, FALSE,
            TRUE, FALSE, FALSE, TRUE, TRUE, FALSE);

INSERT INTO ip_address (value)
VALUES ('Какой-то IP адрес1'),
       ('Какой-то IP адрес2'),
       ('Какой-то IP адрес3'),
       ('Какой-то IP адрес4'),
       ('Какой-то IP адрес5');


INSERT INTO ip_network (value)
VALUES ('Какая-то сеть 1'),
       ('Какая-то сеть 2'),
       ('Какая-то сеть 3'),
       ('Какая-то сеть 4'),
       ('Какая-то сеть 5');

INSERT INTO employee (first_name, middle_name, last_name, telephone, post, individual_tax_number, description,
                      login, email, department_id, role_id, notifications_id)
VALUES ('Bob', '', 'Dillan', 'Nokia', 'Какая-то почта', '05539978852234', 'Люблю рисовать жирафов', 'bodil007',
        'bodil007@mail.com', 4, 2, 1),
       ('Бывший', 'Лучший', 'Самовски', 'SonyEricson', 'Какая-то другая почта', '05539978852235',
        'Отношусь ко всем надменно', 'best', 'best@mail.com', 1, 2, 2),
       ('Jo', '', 'Baiden', 'BlackBerry', 'White House', '0000000001', 'Обожаю спать', 'baida',
        'baida@mail.com', 2, 2, 3),
       ('Kent', '', 'Vermeshel', 'Lg', '', '055399700000234', 'Продам мотоцикл', 'kent',
        'kent@mail.com', 3, 1, 4),
       ('Anna', 'Николаевна', 'Бобровски', 'Раскладушка', '620130', '06663234852234', 'Жизнь удалась',
        'nikolaevna', 'nikolaevna@mail.com', 2, 3, 5);

INSERT INTO employee_access_from_addresses (employee_id, access_from_addresses_id)
VALUES (1, 1),(1, 4),(1, 5),(2, 2),(2, 3),(3, 2),(3, 4),(3, 5),(4, 1),(4, 2),(4, 3),(4, 4),(4, 5),
       (5, 1),(5, 2),(5, 4);

INSERT INTO employee_access_from_network (employee_id, access_from_network_id)
VALUES (1, 1),(1, 2),(2, 1),(2, 3),(3, 1),(3, 3),(4, 1),(4, 2),(4, 3),(4, 4),(4, 5),(5, 1),(5, 5);