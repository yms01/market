CREATE TABLE status
(
    id    BIGINT GENERATED BY DEFAULT AS IDENTITY NOT NULL,
    name  VARCHAR(255),
    color VARCHAR(50),
    CONSTRAINT pk_status PRIMARY KEY (id)
);

CREATE TABLE project
(
    id          BIGINT GENERATED BY DEFAULT AS IDENTITY NOT NULL,
    name        VARCHAR(255),
    code        VARCHAR(255),
    description VARCHAR(2000),
    CONSTRAINT pk_project PRIMARY KEY (id)
);

CREATE TABLE organization
(
    id   BIGINT GENERATED BY DEFAULT AS IDENTITY NOT NULL,
    name VARCHAR(50),
    CONSTRAINT pk_organization PRIMARY KEY (id)
);

CREATE TABLE file
(
    id                  BIGINT GENERATED BY DEFAULT AS IDENTITY NOT NULL,
    added_file          BYTEA,
    name                VARCHAR(50),
    date_and_time_added TIMESTAMP WITHOUT TIME ZONE,
    size                BIGINT,
    edited_by_id        BIGINT,
    CONSTRAINT pk_file PRIMARY KEY (id)
);

CREATE SEQUENCE IF NOT EXISTS hibernate_sequence START WITH 1 INCREMENT BY 1;

CREATE TABLE contract
(
    id                BIGINT  NOT NULL,
    is_removed        BOOLEAN,
    last_edited       TIMESTAMP WITHOUT TIME ZONE,
    number            BIGINT,
    currency_id       INTEGER,
    from_warehouse_id BIGINT,
    counterparty_id   BIGINT,
    sales_channel_id  BIGINT,
    general_access    BOOLEAN NOT NULL,
    status_id         BIGINT,
    comment           VARCHAR(255),
    when_was_created  TIMESTAMP WITHOUT TIME ZONE,
    last_edited_by_id BIGINT,
    organization_id   BIGINT,
    type_of_contract  VARCHAR(50),
    code              INTEGER,
    contract_amount   DOUBLE PRECISION,
    award             FLOAT,
    CONSTRAINT pk_contract PRIMARY KEY (id)
);

ALTER TABLE contract
    ADD CONSTRAINT FK_CONTRACT_ON_COUNTERPARTY FOREIGN KEY (counterparty_id) REFERENCES counterparty (id);

ALTER TABLE contract
    ADD CONSTRAINT FK_CONTRACT_ON_CURRENCY FOREIGN KEY (currency_id) REFERENCES currencies (id);

ALTER TABLE contract
    ADD CONSTRAINT FK_CONTRACT_ON_FROMWAREHOUSE FOREIGN KEY (from_warehouse_id) REFERENCES warehouse (id);

ALTER TABLE contract
    ADD CONSTRAINT FK_CONTRACT_ON_LASTEDITEDBY FOREIGN KEY (last_edited_by_id) REFERENCES employee (id);

ALTER TABLE contract
    ADD CONSTRAINT FK_CONTRACT_ON_ORGANIZATION FOREIGN KEY (organization_id) REFERENCES yurlizo (id);

ALTER TABLE contract
    ADD CONSTRAINT FK_CONTRACT_ON_SALESCHANNEL FOREIGN KEY (sales_channel_id) REFERENCES sales_channel (id);

ALTER TABLE contract
    ADD CONSTRAINT FK_CONTRACT_ON_STATUS FOREIGN KEY (status_id) REFERENCES status (id);

CREATE TABLE task
(
    id          BIGINT GENERATED BY DEFAULT AS IDENTITY NOT NULL,
    name        VARCHAR(2000),
    completed   BOOLEAN                                 NOT NULL,
    executor_id BIGINT,
    term        TIMESTAMP WITHOUT TIME ZONE,
    document_id BIGINT,
    CONSTRAINT pk_task PRIMARY KEY (id)
);

ALTER TABLE task
    ADD CONSTRAINT FK_TASK_ON_EXECUTOR FOREIGN KEY (executor_id) REFERENCES employee (id);

ALTER TABLE task
    ADD CONSTRAINT FK_TASK_ON_DOCUMENT FOREIGN KEY (document_id) REFERENCES document (id);

CREATE TABLE invoice
(
    id                      BIGINT NOT NULL,
    is_removed              BOOLEAN default false,
    last_edited             TIMESTAMP WITHOUT TIME ZONE,
    number                  BIGINT,
    currency_id             INTEGER,
    from_warehouse_id       BIGINT,
    counterparty_id         BIGINT,
    sales_channel_id        BIGINT,
    general_access          BOOLEAN NOT NULL,
    status_id               BIGINT,
    comment                 VARCHAR(2000),
    when_was_created        TIMESTAMP WITHOUT TIME ZONE,
    last_edited_by_id       BIGINT,
    is_conducted            BOOLEAN,
    is_it_buying            BOOLEAN,
    paid_for                INTEGER,
    shipped                 INTEGER,
    accepted                INTEGER,
    is_sent                 BOOLEAN,
    is_printed              BOOLEAN,
    organization_id         BIGINT,
    plan_payment_date       TIMESTAMP WITHOUT TIME ZONE,
    incoming_number         VARCHAR(255),
    date_of_incoming_number TIMESTAMP WITHOUT TIME ZONE,
    warehouse_id            BIGINT,
    contract_id             BIGINT,
    project_id              BIGINT,
    vat                     BOOLEAN,
    vat_in_prise            BOOLEAN,
    interim_result          DOUBLE PRECISION,
    total                   DOUBLE PRECISION,
    CONSTRAINT pk_invoice PRIMARY KEY (id)
);

ALTER TABLE invoice
    ADD CONSTRAINT FK_INVOICE_ON_CONTRACT FOREIGN KEY (contract_id) REFERENCES contract (id);

ALTER TABLE invoice
    ADD CONSTRAINT FK_INVOICE_ON_COUNTERPARTY FOREIGN KEY (counterparty_id) REFERENCES counterparty (id);

ALTER TABLE invoice
    ADD CONSTRAINT FK_INVOICE_ON_CURRENCY FOREIGN KEY (currency_id) REFERENCES currencies (id);

ALTER TABLE invoice
    ADD CONSTRAINT FK_INVOICE_ON_FROMWAREHOUSE FOREIGN KEY (from_warehouse_id) REFERENCES warehouse (id);

ALTER TABLE invoice
    ADD CONSTRAINT FK_INVOICE_ON_LASTEDITEDBY FOREIGN KEY (last_edited_by_id) REFERENCES employee (id);

ALTER TABLE invoice
    ADD CONSTRAINT FK_INVOICE_ON_ORGANIZATION FOREIGN KEY (organization_id) REFERENCES organization (id);

ALTER TABLE invoice
    ADD CONSTRAINT FK_INVOICE_ON_PROJECT FOREIGN KEY (project_id) REFERENCES project (id);

ALTER TABLE invoice
    ADD CONSTRAINT FK_INVOICE_ON_SALESCHANNEL FOREIGN KEY (sales_channel_id) REFERENCES sales_channel (id);

ALTER TABLE invoice
    ADD CONSTRAINT FK_INVOICE_ON_STATUS FOREIGN KEY (status_id) REFERENCES status (id);

ALTER TABLE invoice
    ADD CONSTRAINT FK_INVOICE_ON_WAREHOUSE FOREIGN KEY (warehouse_id) REFERENCES warehouse (id);

CREATE TABLE invoice_file_list
(
    invoice_id   BIGINT NOT NULL,
    file_list_id BIGINT NOT NULL
);

ALTER TABLE invoice_file_list
    ADD CONSTRAINT fk_invfillis_on_file FOREIGN KEY (file_list_id) REFERENCES file (id);

ALTER TABLE invoice_file_list
    ADD CONSTRAINT fk_invfillis_on_invoice FOREIGN KEY (invoice_id) REFERENCES invoice (id);

CREATE TABLE invoice_product_list
(
    invoice_id      BIGINT NOT NULL,
    product_list_id BIGINT NOT NULL
);

ALTER TABLE invoice_product_list
    ADD CONSTRAINT fk_invprolis_on_invoice FOREIGN KEY (invoice_id) REFERENCES invoice (id);

ALTER TABLE invoice_product_list
    ADD CONSTRAINT fk_invprolis_on_product FOREIGN KEY (product_list_id) REFERENCES product (id);

CREATE TABLE invoice_related_documents
(
    invoice_id           BIGINT NOT NULL,
    related_documents_id BIGINT NOT NULL
);

ALTER TABLE invoice_related_documents
    ADD CONSTRAINT fk_invreldoc_on_document FOREIGN KEY (related_documents_id) REFERENCES document (id);

ALTER TABLE invoice_related_documents
    ADD CONSTRAINT fk_invreldoc_on_invoice FOREIGN KEY (invoice_id) REFERENCES invoice (id);

CREATE TABLE invoice_task_list
(
    invoice_id   BIGINT NOT NULL,
    task_list_id BIGINT NOT NULL
);

ALTER TABLE invoice_task_list
    ADD CONSTRAINT fk_invtaslis_on_invoice FOREIGN KEY (invoice_id) REFERENCES invoice (id);

ALTER TABLE invoice_task_list
    ADD CONSTRAINT fk_invtaslis_on_task FOREIGN KEY (task_list_id) REFERENCES task (id);
