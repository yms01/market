CREATE TABLE countries (
                           id  bigserial not null,
                           alpha2 varchar(255) not null,
                           alpha3 varchar(255) not null,
                           full_name varchar(255) not null,
                           is_removed boolean,
                           is_system boolean,
                           numeric_code int4 not null,
                           short_name varchar(255) not null,
                           primary key (id)
)