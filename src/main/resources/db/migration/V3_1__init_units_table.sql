INSERT INTO units (date, digital_code, full_name, is_changeable, is_removed, short_name)
VALUES (now(), 039, 'Дюйм (25,4 мм)', false, false, 'дюйм'),
       (now(), 362, 'Месяц', false, false, 'мес'),
       (now(), 356, 'Час', false, false, 'ч'),
       (now(), 796, 'Штука', false, false, 'шт'),
       (now(), 003, 'Миллиметр', false, false, 'мм')
