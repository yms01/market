CREATE TABLE product_types
(
    id         bigserial not null,
    is_removed boolean,
    name       varchar(255),
    primary key (id)
)