CREATE TABLE units
(
    id            BIGSERIAL NOT NULL,
    date          TIMESTAMP,
    digital_code  INTEGER not null,
    full_name     VARCHAR(255),
    is_changeable BOOLEAN not null,
    is_removed    BOOLEAN not null,
    short_name    VARCHAR(255),
    edited_by_id  BIGINT,
    PRIMARY KEY (id),
    CONSTRAINT fk_units_employee FOREIGN KEY (edited_by_id) REFERENCES employee (id)
)