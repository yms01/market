CREATE TABLE IF NOT EXISTS product
(
    id                                           BIGINT PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY,
    description                                  VARCHAR(255),
    external_code                                VARCHAR(255),
    is_custom_for_every_warehouse                BOOLEAN,
    is_discounts_for_retail_sales_prohibited     BOOLEAN,
    is_min_quantity_same_for_all_warehouses      BOOLEAN,
    is_min_quantity_summarized_in_all_warehouses BOOLEAN,
    is_removed                                   BOOLEAN,
    is_service                                   BOOLEAN,
    is_set                                       BOOLEAN,
    is_traceable                                 BOOLEAN,
    last_edited                                  TIMESTAMP,
    manufacturers_code                           VARCHAR(255),
    min_price                                    NUMERIC(19, 2),
    minimal_quantity_at_stock                    INTEGER,
    name                                         VARCHAR(255),
    purchase_price                               NUMERIC(19, 2),
    unique_code                                  VARCHAR(255),
    volume                                       INTEGER,
    weight                                       INTEGER,
    country_id                                   BIGINT CONSTRAINT country_fkey REFERENCES country,
    group_of_products_id_group                   BIGINT CONSTRAINT group_of_products_fkey REFERENCES group_of_products,
    last_edited_by_id                            BIGINT CONSTRAINT employee_fkey REFERENCES employee,
    packaging_id                                 INTEGER CONSTRAINT packaging_fkey REFERENCES packaging,
    product_type_id                              BIGINT CONSTRAINT product_types_fkey REFERENCES product_types,
    sign_of_the_subject_of_calculation_id        BIGINT CONSTRAINT sign_of_the_subject_of_calculation_fkey REFERENCES sign_of_the_subject_of_calculation,
    supplier_id                                  BIGINT CONSTRAINT counterparty_fkey REFERENCES counterparty,
    taxation_system_id                           BIGINT CONSTRAINT taxation_system_fkey REFERENCES taxation_system,
    type_of_accounting_id                        BIGINT CONSTRAINT type_of_accounting_fkey REFERENCES type_of_accounting,
    unit_of_measurement_id                       BIGINT CONSTRAINT units_fkey REFERENCES units,
    vat_id                                       BIGINT CONSTRAINT vat_fkey REFERENCES vat
);

CREATE TABLE product_images_links
(
    product_id      BIGINT NOT NULL CONSTRAINT product_fkey REFERENCES product,
    images_links    VARCHAR(2048)
);

CREATE TABLE product_prices_map
(
    product_id  BIGINT NOT NULL CONSTRAINT product_fkey REFERENCES product,
    price_value NUMERIC(19, 2),
    price_name  VARCHAR(255) NOT NULL,
    PRIMARY KEY (product_id, price_name)
);

CREATE TABLE product_sets_map
(
    product_set_id BIGINT NOT NULL CONSTRAINT product_fkey REFERENCES product,
    quantity       INTEGER,
    product_id     BIGINT NOT NULL,
    PRIMARY KEY (product_set_id, product_id)
);

CREATE TABLE product_warehouse_id_min_quantity_map
(
    product_id   BIGINT NOT NULL CONSTRAINT product_fkey REFERENCES product,
    min_quantity INTEGER,
    warehouse_id BIGINT NOT NULL,
    PRIMARY KEY (product_id, warehouse_id)
);