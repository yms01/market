INSERT INTO orders (id, comment, general_access, is_removed, last_edited, number, when_was_created,
                    counterparty_id, currency_id, from_warehouse_id, last_edited_by_id, sales_channel_id, status_id,
                    conducted, date_of_acceptance, expectation, external_code, is_buying, reserve,
                    address_id, contract_id, organization_id, project_id
                    )
VALUES (default,'comment1', true, false, now(), 1, now(),
        1, 1, 1, 1, 1, 1,
        false, '2023-01-01 11:11', true, 123, true, false,
        1, 1, 1, 1
        ),
       (default, 'comment2', true, false, now(), 2, now(),
        1, 1, 1, 1, 1, 1,
        false, '2023-01-01 11:11', true, 534, true, false,
        1, 1, 1, 1
        ),
       (default, 'comment3', true, false, now(), 3, now(),
        1, 1, 1, 1, 1, 1,
        false, '2023-01-01 11:11', true, 645, true, false,
        1, 1, 1, 1
        ),
       (default, 'comment4', true, false, now(), 4, now(),
        1, 1, 1, 1, 1, 1,
        false, '2023-01-01 11:11', true, 333, true, false,
        1, 1, 1, 1
        ),
       (default, 'comment5', true, false, now(), 5, now(),
        1, 1, 1, 1, 1, 1,
        false, '2023-01-01 11:11', true, 764, true, false,
        1, 1, 1, 1
        );

INSERT INTO orders_files (order_id, files_id)
VALUES (1, 1),
       (2, 1),
       (3, 1),
       (4, 1),
       (5, 1);

INSERT INTO orders_products (order_id, products_id)
VALUES (1, 1),
       (2, 1),
       (3, 1),
       (4, 1),
       (5, 1);

INSERT INTO orders_tasks (order_id, tasks_id)
VALUES (1, 1),
       (2, 1),
       (3, 1),
       (4, 1),
       (5, 1)