INSERT INTO product (description, external_code, is_custom_for_every_warehouse,
                     is_discounts_for_retail_sales_prohibited, is_min_quantity_same_for_all_warehouses,
                     is_min_quantity_summarized_in_all_warehouses, is_removed, is_service, is_set, is_traceable,
                     last_edited, manufacturers_code, min_price, minimal_quantity_at_stock, name, purchase_price,
                     unique_code, volume, weight, country_id, group_of_products_id_group, last_edited_by_id,
                     packaging_id, product_type_id, sign_of_the_subject_of_calculation_id, supplier_id,
                     taxation_system_id, type_of_accounting_id, unit_of_measurement_id, vat_id)
VALUES ('description 1', 'external code 1', TRUE, FALSE, FALSE, FALSE, FALSE, FALSE, TRUE, FALSE, NOW(),
        'manufacturers_code 1', 50, NULL, 'price name 1', 40, 'unique_code 1', 35, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
       ('description 2', 'external code 2', TRUE, FALSE, FALSE, FALSE, FALSE, TRUE, FALSE, FALSE, NOW(),
        'manufacturers_code 2', 50, NULL, 'price name 2', 40, 'unique_code 2', 8, 23, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
       ('description 3', 'external code 3', TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, NOW(),
        'manufacturers_code 3', 50, NULL, 'price name 3', 40, 'unique_code 3', 4, 11, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
       ('description 4', 'external code 4', TRUE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, NOW(),
        'manufacturers_code 4', 50, NULL, 'price name 4', 40, 'unique_code 4', 5, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
       ('description 5', 'external code 5', TRUE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, NOW(),
        'manufacturers_code 5', 50, NULL, 'price name 5', 40, 'unique_code 5', 33, 56, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);


INSERT INTO product_images_links (product_id, images_links)
VALUES (1, 'image link 1'),
       (1, 'image link 2'),
       (2, 'image link 1'),
       (2, 'image link 2'),
       (3, 'image link 1'),
       (3, 'image link 2'),
       (4, 'image link 1'),
       (4, 'image link 2'),
       (5, 'image link 1'),
       (5, 'image link 2');

INSERT INTO product_prices_map (product_id, price_value, price_name)
VALUES (1, 10, 'price1'),
       (1, 20, 'price2'),
       (2, 10, 'price1'),
       (2, 20, 'price2'),
       (3, 10, 'price1'),
       (3, 20, 'price2'),
       (4, 10, 'price1'),
       (4, 20, 'price2'),
       (5, 10, 'price1'),
       (5, 20, 'price2');

INSERT INTO product_sets_map (product_set_id, quantity, product_id)
VALUES (1, 9, 1);

INSERT INTO product_warehouse_id_min_quantity_map (product_id, min_quantity, warehouse_id)
VALUES (3, 10, 1);